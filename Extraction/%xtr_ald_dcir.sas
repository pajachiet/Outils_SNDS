
/************************************************************************************************
*	Macro  : xtr_ald_dcir																		*
*	*********************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*
*	*********************************************************************************************
*	Mises a jour : (le cas echeant : preciser date, auteur, et objet de la MAJ)					*
*		 2/11/2018 - MA : conversion resultats IMB_ALD_DTD et IMB_ALD_DTF en format ddmmyy10.	*
*							+ modification filtres sur dates pour optimisation requete			*
*		23/11/2018 - MA : MAJ suite modification %multi_like									*
*		27/06/2019 - MA : recherche complementaires dans les donn�es prestations pour les     	*
*							personnes ayant eu un motif d'exon�ration de soins li� � une ALD et *
*							non referencees dans le r�f�rentiel m�dicalis�e (IR_IMB_R)			*
*							+ changement de la variable d'identification du code ALD d�sormais  *
*							  r�cup�r�e dans la table r�f�rentiel des ALD (IR_CIM_V)			*
* 		07/11/2022 - PYD : D�finition automatique ann�e archivage donn�es prestation			*
* 		03/08/2023 - CM : Simplification, MAJ Filtre Opt_exo + Documentation					*
*	*********************************************************************************************
*	Description : Cette macro permet d'extraire, dans les donn�es du DCIR et durant une 		*
*                 p�riode de temps donn�e, les ALD en cours.									*
*				  Si aucun code (CIM-10, ALD) n'est renseign�, la macro extrait les           	*
*                 identifiants des personnes qui sont en ALD (quelconque) dans cette p�riode 	*
*	*********************************************************************************************
*	Parametres :																				*
*		lib_prj 	: nom de la biblioth�que o� sera rang�e la table de sortie					*
*		tab_out 	: nom de la table de sortie													*
*		ann_deb 	: annee de d�but de la p�riode d'extraction (format YYYY)					*
*		ann_fin 	: ann�e de fin de la p�riode d'extraction (format YYYY)						*
*		opt_pop 	: optionnel : table de population � "jointer" (ex. POP)						*
*		opt_cim_inc : optionnel : liste des codes CIM-10 � extraire			                 	*
*		opt_cim_exc : optionnel : liste des codes CIM-10 � ne pas extraire		            	*
*		opt_ald 	: optionnel : liste des numero d'ALD � selectionner		                	*
*		opt_exo		: optionnel : permet de filtrer sur le type de prise en charge (valeurs 41  *
*						42, 43, 44, 45 et 46 ; 1-oui)											*
*	*********************************************************************************************
*	R�sultat : table contenant les variables suivantes :										*
*		BEN_NIR_PSA      : identifiant b�n�ficiaire                                             *
*       BEN_RNG_GEM      : rang b�n�ficiaire                                                    *
*		MED_MTF_COD	     : code CIM-10					                                    	*
*		ALD_030_COD	     : num�ro d'ALD							                         		*
*		IMB_ALD_DTD	     : date de d�but			                                    		*
*		IMB_ALD_DTF	     : date de fin					                        				*
*	*********************************************************************************************
*	Exemple :																					*
*		%xtr_ald_dcir(  																		*
*			lib_prj = SASDATA1,  																*
*			tab_out = EXTRACTION_ALD,  															*
*			ann_deb = 2009,  																	*
*			ann_fin = 2013,  																	*
*           opt_pop = POP_65,  																	*
*			opt_cim_inc = M75,  																*
*			opt_cim_exc = M751,  																*
*			opt_ald =,	  																		*
*			opt_exo = 1																			*
*		);																						*
*	Cet exemple permet d'extraire toutes les ALD de 2009 � 2013	chez les sujets identifi�s dans *
*	la population POP_65 stock�e dans la librairie SASDATA1 ou ORAUSER. Sont extraites toutes 	*
*	les ALD cod�es M75 sauf M751, selon le type d'exon�ration de l'ALD. Les donn�es sont 		*
*	stock�es dans la table EXTRACTION_ALD dans la librairie SASDATA1.							*
************************************************************************************************/

options nosource nonotes nosymbolgen;
option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;

/*Macro multi-like*/
%macro multi_like(var, lst_cod, aim, stp);

	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);

		%let val = %qscan(&lst_cod., &i., %str( ));

		%if %sysfunc(verify(&val., '0123456789')) > 0 %then %do;
			%if &aim. = INC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
				%end;
				%let ope = or;
			%end;

			%if &aim. = EXC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
				%end;
				%let ope = and;
			%end;
		%end;

		%if %sysfunc(verify(&val., '0123456789')) = 0 %then %do;
			%if &aim. = INC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) = (&val.)))
				%end;
				%put %str(&ope. (&var.) = (%superq(val)));	
				%let ope = or;
			%end;
			%if &aim. = EXC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) ^= (&val.)))
				%end;
				%put %str(&ope. (&var.) ^= (%superq(val)));
				%let ope = and;
			%end;
		%end;	

		%let i = %eval(&i. + 1);
	%end;

%mend;

%macro xtr_ald_dcir(lib_prj, tab_out, ann_deb, ann_fin, opt_pop, opt_cim_inc, opt_cim_exc, opt_ald, opt_exo);

/*Macros suppression tables ORACLE*/
	%macro checkDrop(myTable);
		%if %SYSFUNC(exist(orauser.&myTable)) %then %do;
		    proc sql;
			%connectora;
			EXECUTE(
		   		drop table &myTable
			)
			BY ORACLE;
		    quit;
		%end;
	%mend;

/*copie de la table population de la librairie du projet vers orauser*/
	%macro copie_POP();
		%if %sysfunc(exist(orauser.&opt_pop.))=0 %then %do; 
			proc sql;
				create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) as
				select *
				from &lib_prj..&opt_pop.;
			quit;
		%end;
		%else %do;
			%checkDrop(&opt_pop.);
			proc sql;
				create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) as
				select *
				from &lib_prj..&opt_pop.;
			quit;
		%end;

	%mend;
	%copie_POP();


	/* Definition automatique annee archivage des donnees de prestation */
	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let aa = 2006;
	%let exist = 1;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(oravue.er_prs_f_&aa.))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_arc = %eval(&aa.-1);


	/* Parametrage du filtre de jointure avec la population */
	%if &opt_pop. > 0 %then %do;
		%let jnt_tab = '&opt_pop. t_pop inner join IR_IMB_R t_ald on t_pop.BEN_NIR_PSA = t_ald.BEN_NIR_PSA
						inner join IR_CIM_V t_ref on substr(t_ald.MED_MTF_COD, 1, 4) = substr(t_ref.CIM_COD, 1, 4)';
	%end;
	%else %do;
		%let jnt_tab = 'IR_IMB_R t_ald inner join IR_CIM_V t_ref on substr(t_ald.MED_MTF_COD, 1, 4) = substr(t_ref.CIM_COD, 1, 4)';
	%end;

	/* Parametrage des filtres sur les codes CIM-10 ou ALD */
	%if &opt_cim_inc. > 0 %then %do;
		%if &opt_cim_exc. > 0 %then %do;
			%if &opt_ald. > 0 %then %do;
				%let flt_ald = '(%multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) or %multi_like(var = t_ref.ALD_030_COD, lst_cod = &opt_ald., aim = INC, stp = PROC) and %multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) and';
			%end;
			%else %do;
				%let flt_ald = '(%multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) and';
			%end;
		%end;
		%else %do;
			%if &opt_ald. > 0 %then %do;
				%let flt_ald = '(%multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) or %multi_like(var = t_ref.ALD_030_COD, lst_cod = &opt_ald., aim = INC, stp = PROC)) and';
			%end;
			%else %do;
				%let flt_ald = '(%multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and';
			%end;
		%end;
	%end;
	%else %do;
		%if &opt_cim_exc. > 0 %then %do;
			%if &opt_ald. > 0 %then %do;
				%let flt_ald = '(%multi_like(var = t_ref.ALD_030_COD, lst_cod = &opt_ald., aim = INC, stp = PROC) and %multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) and';
			%end;
			%else %do;
				%let flt_ald = '(%multi_like(var = t_ald.MED_MTF_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) and';
			%end;
		%end;
		%else %do;
			%if &opt_ald. > 0 %then %do;
				%let flt_ald = '(%multi_like(var = t_ref.ALD_030_COD, lst_cod = &opt_ald., aim = INC, stp = PROC)) and';
			%end;
			%else %do;
				%let flt_ald = '';
			%end;
		%end;
	%end;

/* Parametrage du filtre des ALD exonerees ou tout venant */
	%if &opt_exo. = 1 %then %do;
		%let flt_exo = 't_ald.IMB_ETM_NAT in (41, 42, 43, 44, 45, 46) and';
	%end;
	%else %do;
		%let flt_exo = '';
	%end;

/* ----------------- */
/* -     MACRO     - */
/* ----------------- */

/* Initialisation des parametres */
	%let dateDbt = '01/01/&ann_deb.';
	%let date_dbt = %sysfunc(compress(&dateDbt., '/'));
	%let dateFin = '31/12/&ann_fin.';
	%let date_fin = %sysfunc(compress(&dateFin., '/'));

	%checkDrop(&tab_out._IMB);

/* Extraction des ALD selectionnees a partir de la table ALD */
	proc sql;
		%connectora;
		EXECUTE(
			create table &tab_out._IMB as
			select distinct t_ald.BEN_NIR_PSA,
				t_ald.MED_MTF_COD,
				t_ald.IMB_ALD_DTD,
				t_ald.IMB_ALD_DTF,
				t_ref.ALD_030_COD,
				t_ald.IMB_ETM_NAT
			from %scan(&jnt_tab., 1, "'")
			where %scan(&flt_ald., 1, "'")
				%scan(&flt_exo., 1, "'")
				t_ald.IMB_ALD_DTD <= to_date(%str(%'&date_fin.%'), 'ddmmyyyy') and
				(t_ald.IMB_ALD_DTF >= to_date(%str(%'&date_dbt.%'), 'ddmmyyyy') or t_ald.IMB_ALD_DTF is NULL)
		)
		BY ORACLE;
	quit;


	proc sql;
		create table &lib_prj..&tab_out._IMB as
		select BEN_NIR_PSA,
				MED_MTF_COD,
				datepart(IMB_ALD_DTD) as IMB_ALD_DTD format ddmmyy10.,
				datepart(IMB_ALD_DTF) as IMB_ALD_DTF format ddmmyy10.,
				ALD_030_COD,
				IMB_ETM_NAT
		from orauser.&tab_out._IMB;
	quit;


/* Recherche des ALD dans les prestations de soins */
/* Recherche lancee uniquement si absence de filtre sur le code CIM-10 ou ALD car les donnees prestations ne renseignent pas precisement cette information */

%if (&opt_cim_inc. = and &opt_cim_exc. = and &opt_ald. = ) %then %do;

	/* 2-1 - Initialisation de la table d'extraction */
	%checkDrop(&tab_out._PRS_intrm);

	data orauser.&tab_out._PRS_intrm;
	set orauser.&tab_out._IMB (obs=0);
	run;

	%checkDrop(&tab_out._IMB);

	/* 2-2 - Extraction dans les donnees PRS */
	%do aaaa = %sysfunc(max(&ann_deb., 2006)) %to &ann_fin.;
		%if &aaaa. <= &ann_arc. %then %do;
			%let T_PRS = ER_PRS_F_&aaaa.;
		%end;
		%if &aaaa. > &ann_arc. %then %do;
			%let T_PRS = ER_PRS_F;
		%end;

		%if &opt_pop. > 0 %then %do;
			%let jnt_tab_prs = '&opt_pop. t_pop inner join &T_PRS. t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA';
		%end;
		%else %do;
			%let jnt_tab_prs = '&T_PRS. t_prs';
		%end;

		%do i = 0 %to 38;
			%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
			%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

			proc sql;
				%connectora;
				EXECUTE(
					%if %sysfunc(exist(&tab_out._PRS_intrm))=0 %then %do;
						create table &tab_out._PRS_intrm as
					%end;
					%else %do;
						insert into &tab_out._PRS_intrm
					%end;
					select distinct t_prs.BEN_NIR_PSA,
						case 
							when t_prs.EXO_MTF in (41, 42) then 88		/* Exoneration pour l'un des 31 ALD listees mais impossible de savoir laquelle precisement => inconnue */
							when t_prs.EXO_MTF in (43, 44) then 31
							when t_prs.EXO_MTF in (45, 46) then 32
						end as ALD_030_COD,
						datepart(t_prs.EXE_SOI_DTD) as IMB_ALD_DTD format ddmmyy10.
						NULL as IMB_ALD_DTF format ddmmyy10.,
						NULL as MED_MTF_COD
					from %scan(&jnt_tab_prs., 1, "'")
					where
						t_prs.EXE_SOI_DTD between to_date(%str(%'&date_dbt.%'), 'ddmmyyyy') and to_date(%str(%'&date_fin.%'), 'ddmmyyyy') and
						(t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') - 1) and
						t_prs.EXO_MTF in (41, 42, 43, 44, 45, 46)
				)
				BY ORACLE;
			quit;
		%end;

		/* Copie de la table vers la librairie de sortie */
		proc sql;
			create table &lib_prj..&tab_out._PRS_intrm as
			select *
			from orauser.&tab_out._PRS_intrm;
		quit;

		%checkDrop(&tab_out._PRS_intrm);
	%end;

	/* 2-3 - Calcul des dates approximatives de dbut et fin des ALD extraites */
	proc sql;
		create table &lib_prj..&tab_out._PRS as
		select BEN_NIR_PSA,
			MED_MTF_COD,
			ALD_030_COD,
			min(IMB_ALD_DTD) as IMB_ALD_DTD format ddmmyy10.,
			max(IMB_ALD_DTD) as IMB_ALD_DTF format ddmmyy10.
		from &lib_prj..&tab_out._PRS_intrm
		group by BEN_NIR_PSA, MED_MTF_COD, ALD_030_COD;
	quit;

	/*Copie table initiale des ALD*/
	data &lib_prj..&tab_out._intrm;
	set &lib_prj..&tab_out._IMB;
	run;

	/* 2-4 - Fusion avec les donnes issues du refrentiel medicalise */
	proc sql;
		create table &lib_prj..&tab_out._IMB as
		select distinct 
			case 
				when t_ald_ref.BEN_NIR_PSA ^= '' then t_ald_ref.BEN_NIR_PSA
				else t_ald_prs.BEN_NIR_PSA
			end 
			as BEN_NIR_PSA,
			case 
				when t_ald_ref.MED_MTF_COD ^= '' then t_ald_ref.MED_MTF_COD
				else t_ald_prs.MED_MTF_COD
			end 
			as MED_MTF_COD,
			case 
				when t_ald_ref.ALD_030_COD ^= . then t_ald_ref.ALD_030_COD
				else t_ald_prs.ALD_030_COD
			end 
			as ALD_030_COD,
			case 
				when t_ald_ref.IMB_ALD_DTD ^= . then t_ald_ref.IMB_ALD_DTD
				else t_ald_prs.IMB_ALD_DTD
			end 
			as IMB_ALD_DTD,
			case 
				when t_ald_ref.IMB_ALD_DTF ^= . then t_ald_ref.IMB_ALD_DTF
				else t_ald_prs.IMB_ALD_DTF
			end 
			as IMB_ALD_DTF
		from &lib_prj..&tab_out._intrm t_ald_ref full join &lib_prj..&tab_out._PRS t_ald_prs on t_ald_ref.BEN_NIR_PSA = t_ald_prs.BEN_NIR_PSA;
	quit;

	/* 2-2-5 - Nettoyage des tables intermdiaires */
	proc datasets library=&lib_prj.;
		delete &tab_out._PRS &tab_out._PRS_intrm &tab_out._intrm;
	run;

%end;


/* 3 - Finalisation de la table de sortie */

/***
Mise a jour de la date de fin d'ALD lorsque celle-ci est renouvellee
***/
proc sql;
	create table &lib_prj..&tab_out. as
	select BEN_NIR_PSA,
		MED_MTF_COD,
		ALD_030_COD,
		IMB_ALD_DTD format ddmmyy10.,
		max(IMB_ALD_DTF) as IMB_ALD_DTF format ddmmyy10.,
		IMB_ETM_NAT
	from &lib_prj..&tab_out._IMB
	group by BEN_NIR_PSA, MED_MTF_COD, ALD_030_COD, IMB_ALD_DTD;
quit;

proc sql;
	drop table &lib_prj..&tab_out._IMB;
quit;

/* 2-2-5 - Nettoyage des tables intermdiaires */
%checkDrop(&tab_out._IMB);
%checkDrop(&opt_pop.);

%mend;
