/****************************************************************************************************************
*	Macro  : xtr_hsp_dcir																						*
*	Date   : 05 avr 2019																						*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises a jour :																								*
*		12/04/2019 - CM : - ajout de l option opt_cim_dgn_ass qui permet d extraire les CIM-10 selon les		*
*							  diagnotics principaux, associes ou relies 										*
*		                    - correction du probleme de concatenation des tables annuelles						*
*		01/08/2019 - MA : suppression des "" associes avec la fonction %index qui l'empechait de fonctionner	*
*						  lorsqu'au moins deux caracteres etaient renseignes dans le parametre mod_hsp			*
*		05/09/2019 - CM : Diagnostics associes ajoutes pour les hospitalisations PSY							*
*		01/10/2020 - EH : Correction de la fusion avec les tables de diagnostics associes par des left join		*
*		01/10/2020 - PYD : Requetes en Pass Through Explicite : passage vers oracle								* 
*		22/05/2023 - CM : Mise au propre des commentaires + MAJ des filtres MCO	+ DP RUM						* 	 	
*	*************************************************************************************************************
*	Description : Cette macro permet d'extraire, dans les donnees du PMSI par annee donnee, les					*
*				hospitalisations identifiees a partir des codes CIM-10.											*
*				Si aucun code CIM-10 n'est renseigne, la macro extrait toutes les hospitalisations realisees	*
*				sur la periode donnee, par annee, uniquement si une population est renseignee.					*
*																												*
*				Plusieurs options sont disponibles afin de peaufiner la recherche sur les hospitalisations.		*
*				Elles ne sont donc pas obligatoires, et sont listees dans les parametres ci-dessous				*
*	*************************************************************************************************************
*	Parametres :																								*
*		lib_out 	: nom de la bibliotheque ou sera rangee la table de sortie									*
*		tab_out 	: nom de la table de sortie																	*
*		ann_deb 	: annee de debut de la periode d'extraction (format YYYY)									*
*		ann_fin 	: annee de fin de la periode d'extraction (format YYYY)										*
*		mod_hsp 	: extraction selon les lettres : M, H, S et/ou P											*
*																												*
*		opt_pop 	: optionnel : table de population a "jointer" (ex. SASDATA1.POP)							*
*		opt_dgn 	: optionnel : Extraction des codes associes et relies (valeur : 1-oui)						*
*		opt_cim_inc : optionnel : liste des codes CIM-10 des hospitalisations a extraire						*
*		opt_cim_exc : optionnel : liste des codes CIM-10 des hospitalisations a ne pas extraire					*
*		opt_cim_dgn_ass : optionnel : Extraction des hospitalisations selon les codes associes et relies,		*
*									  en plus du diagnostic principal (valeur : 1-oui)							*
*		opt_dpl_rum : optionnel : Extraction des diagnostics principaux des Resumes Unites Medicales			*
*								  (valeur : 1-oui). Entraine une modification sur les options opt_dgn et		*
*								  opt_cim_dgn_ass qui passent � une valeur 0 - Non								*
*		opt_suppr 	: optionnel : Supprime les tables annuelles d'hospitalisation (1-oui)						*
*		opt_conc 	: optionnel : Concatene les tables annuelles d'hospitalisation (1-oui)						*
*		opt_conc_ogn 	: optionnel : Supprime les tables groupees selon l'origine d'hospitalisation (1-oui)	*
*		opt_suppr_ogn 	: optionnel : Concatene les tables groupees selon l'origine d'hospitalisation (1-oui)	*
*	*************************************************************************************************************
*	Remarques																									*
*		La liste des codes doit contenir des espaces entre chaque valeur, exemple : opt_cim_inc=C7 C9			*
*	*************************************************************************************************************
*	Resultat : table contenant les variables suivantes :														*
*		NIR_ANO_17       : numero d'identifiant de la personne                          						*
*		ETA_NUM     	 : numero de l'etablissement (numero FINESS)                      						*
*		RSA_NUM      	 : numero de l'index du RSA/RHAD/etc.                            						*
*		EXE_SOI_DTD      : date de debut d'hospitalisation                           							*
*		EXE_SOI_DTF      : date de fin d'hospitalisation                            							*
*		SEJ_NBJ 	     : duree de l'hospitalisation en jours													*
*		DGN_PAL 	     : code CIM_10 du diagnostic principal d'hospitalisation								*
*		ASS_DGN 	     : code CIM_10 du ou des diagnostics associe(s) a l'hospitalisation						*
*		SOR_MOD 	     : mode de sortie d'hospitalisation														*
*		HSP_OGN    		 : origine de l'hospitalisation (MCO, HAD, SSR, PSY)                         			*
*	*************************************************************************************************************
*	Exemple :																									*
*	%xtr_hsp_dcir(lib_out=work,tab_out=hospit,ann_deb=2013,ann_fin=2016,mod_hsp=MH,opt_cim_inc=C7 C9,			*
*                opt_cim_exc=,opt_dgn=1,opt_cim_dgn_ass=,opt_pop=,opt_suppr=,opt_conc=1,opt_suppr_ogn=,			*
*				 opt_conc_ogn=1);																				*
*																												*
*   Dans cet exemple, seront extraites dans une table unique les hospitalisations MCO et HAD de 2013 a 2016,	*
*	avec les codes diagnositcs associes de chaque hospitalisation ayant pour code CIM-10 : C7 ou C9, sans 		*
*	supprimer les tables interm�diaires par type d'hospitalisation												*
****************************************************************************************************************/

/*Macro multi-like - macro permettant de realiser un equivalent de la fonction %like% */
%macro multi_like(var, lst_cod, aim, stp);

	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);

		%let val = %qscan(&lst_cod., &i., %str( ));

		%if %sysfunc(verify(&val., '0123456789')) > 0 %then %do;
			%if &aim. = INC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) =: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) =: %'%upcase(%superq(val))%');
				%end;
				%let ope = or;
			%end;

			%if &aim. = EXC %then %do;
				%if &stp. = PROC %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;
					%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%end;
				%if &stp. = DATA %then %do;
					%if %superq(val) > 0 %then %do;
					  %cmpres(%str(&ope. %upcase(&var.) ^=: %'%upcase(&val.)%'))
					%end;
					%put %str(&ope. %upcase(&var.) ^=: %'%upcase(%superq(val))%');
				%end;
				%let ope = and;
			%end;
		%end;

		%if %sysfunc(verify(&val., '0123456789')) = 0 %then %do;
			%if &aim. = INC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) = (&val.)))
				%end;
				%put %str(&ope. (&var.) = (%superq(val)));	
				%let ope = or;
			%end;
			%if &aim. = EXC %then %do;
				%if %superq(val) > 0 %then %do;
				  %cmpres(%str(&ope. (&var.) ^= (&val.)))
				%end;
				%put %str(&ope. (&var.) ^= (%superq(val)));
				%let ope = and;
			%end;
		%end;	

		%let i = %eval(&i. + 1);
	%end;

%mend;




%macro xtr_hsp_dcir(lib_out=,tab_out=,ann_deb=,ann_fin=,mod_hsp=,opt_cim_inc=,opt_cim_exc=,opt_dgn=,opt_cim_dgn_ass=,opt_dpl_rum=,opt_pop=,opt_suppr=,opt_conc=,opt_conc_ogn=,opt_suppr_ogn=);

	option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;
/*Macros suppression tables ORACLE*/
	%macro checkDrop(myTable);
		%if %SYSFUNC(exist(orauser.&myTable)) %then %do;
		    proc sql;
			%connectora;
			EXECUTE(
		   		drop table &myTable
			)
			BY ORACLE;
		    quit;
		%end;
	%mend;

	%macro copie_POP(lib_prj);
		/*copie de la table population de la librairie du projet vers orauser*/
		%if &opt_pop. > 0 %then %do;
			%if %sysfunc(exist(orauser.&opt_pop.))=0 %then %do; 
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
				    as select * from &lib_prj..&opt_pop.;
				quit;
			%end;
			%else %do;
				%checkDrop(&opt_pop.);
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
				    as select * from &lib_prj..&opt_pop.;
				quit;
			%end;
		%end;
	%mend;

	%copie_POP(lib_prj=&lib_out.);

/* Messages d'erreur */
	%if &mod_hsp. = %then %do;
		%put ERROR: Le macro parametre mod_hsp est obligatoire. Indiquez les lettres des types d hospitalisations que vous voulez extraire, M, H, S, P;
		%goto Stop_macro;
	%end;
	%if &ann_deb. = or &ann_fin. = %then %do;
		%put ERROR: Indiquez un intervalle de temps en annee de minimum 1 an, en completant a la fois la variable ann_deb (i.e. Annee de debut de l extraction) et ann_fin (i.e. Annee de fin de l extraction, incluse).;
		%goto Stop_macro;

	%end;
	%if &lib_out. = or &tab_out. = %then %do;
		%put ERROR: Les parametres lib_out et tab_out sont obligatoires afin de recuperer les donnees dans une table. Veuillez les completer !;
		%goto Stop_macro;
	%end;
	%if &opt_suppr. = 1 and &opt_conc. NE 1 %then %do;
		%put ERROR: Attention, aucune donnee ne peut etre enregistree puisque vous supprimez les tables intermediaires sans les concatener. Pensez a passer le parametre opt_conc a 1 si vous voulez supprimer les tables intermediaires.;
		%goto Stop_macro;
	%end;
	%if &opt_cim_inc. = and &opt_cim_exc. = and &opt_pop. = %then %do;
		%put ERROR: Sans au moins un des parametres completes, vous allez extraire toute la table des hospitalisations. Nous preferons ne pas permettre cette operation : une simple Proc SQL permet de dupliquer la table reference;
		%goto Stop_macro;
	%end;
	%if &opt_dgn. NE 1 and &opt_cim_dgn_ass. = 1 %then %do;
		%put ERROR: Pour extraire les CIM-10 selon les diagnostics principaux, associes/relies, vous devez mettre loption opt_dgn a 1 pour aussi extraire les diagnotics associes dans vos tables de sortie.;
		%goto Stop_macro;
	%end;
	%if %length(&opt_cim_inc.) = 0 and %length(&opt_cim_exc.) = 0 and &opt_cim_dgn_ass. = 1 %then %do;
		%put ERROR: Vous ne pouvez pas extraire les donnees d hospitalisation selon les diagnostics associes/relies sans inclure ou exclure des codes CIM-10. Si vous voulez extraire les informations sur les diagnotics associes et relies en plus du diagnostic principal, cela correspond a l option opt_dgn.;
		%goto Stop_macro;
	%end;	/* Fin des messages d'erreur */


/* Choix de la population */
	%if &opt_pop. > 0 %then %do;
		%let jnt_mco_pop = '&opt_pop. T_POP inner join T_MCO&aa.C T_MCOC on T_POP.BEN_NIR_PSA = T_MCOC.NIR_ANO_17';
		%let jnt_psy_pop = '&opt_pop. T_POP inner join T_RIP&aa.C T_RIPC on T_POP.BEN_NIR_PSA = T_RIPC.NIR_ANO_17';
		%let jnt_had_pop = '&opt_pop. T_POP inner join T_HAD&aa.C T_HADC on T_POP.BEN_NIR_PSA = T_HADC.NIR_ANO_17';
		%let jnt_ssr_pop = '&opt_pop. T_POP inner join T_SSR&aa.C T_SSRC on T_POP.BEN_NIR_PSA = T_SSRC.NIR_ANO_17';
	%end;
	%else %do;
		%let jnt_mco_pop = 'T_MCO&aa.C T_MCOC';
		%let jnt_psy_pop = 'T_RIP&aa.C T_RIPC';
		%let jnt_had_pop = 'T_HAD&aa.C T_HADC';
		%let jnt_ssr_pop = 'T_SSR&aa.C T_SSRC';
	%end;	/* Fin du choix de la population */	

/* Initialisation des parametres lors d'une requete sur les tables UM */
	%if &opt_dpl_rum. = 1 and &mod_hsp. ne M %then %do;
		%put WARNING: Etant donne que vous avez indique vouloir travailler sur les RUM (variable opt_dpl_rum valant 1), nous avons selectionne uniquement une extraction sur les MCO. Pour rappel, vous avez indique vouloir extraire les motifs d hospitalisations autres que MCO avec la variable mod_hsp.;
		%put WARNING: Etant donne que vous avez indique vouloir travailler sur les RUM (variable opt_dpl_rum valant 1), aucune extraction n est pas principe utile de realiser sur les diagnostics associes/relies, les options correspondantes ont ete modifiees pour etre nulles.;
		%let mod_hsp = M;
		%let opt_dgn=0;
		%let opt_cim_dgn_ass=0;
		%let opt_conc_ogn=0;
		%let opt_suppr_ogn=0;
	%end;

/* Boucle par annee */
%let ann_act = %sysfunc(year(%sysfunc(today())));
%do aaaa = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
	%let aa = %substr(&aaaa., 3, 2);


/* Choix de la liste des CIM-10 selon les modalites completes dans l'appel de la macro */
	/* Selection des filtres diagnostics pour les hospitalisations hors DP RUM */
		%if &opt_dpl_rum. ne 1 %then %do;
			%if %length(&opt_cim_inc.) > 0 %then %do;
				%if %length(&opt_cim_exc.) > 0 %then %do;	
					%if &opt_cim_dgn_ass. > 0 %then %do;	
						%let flt_dgn_cim_m_pal = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';					
						%let flt_dgn_cim_m_rel = '(%multi_like(var = T_MCOB.DGN_REL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_MCOB.DGN_REL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
						%let flt_dgn_cim_m_ass = '(%multi_like(var = T_MCOD.ASS_DGN, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_MCOD.ASS_DGN, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_p_pal = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s_pal = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s_ass = '(%multi_like(var = T_SSRD.DGN_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_SSRD.DGN_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
							%let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADDMPA.DGN_ASS_MPA, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_HADDMPA.DGN_ASS_MPA, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
								   %let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADD.DGN_ASS, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_HADD.DGN_ASS, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
					%end;					
					%else %do;		
						%let flt_dgn_cim_m = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_p = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC) and %multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
					%end;
				%end;
				%else %do;
					%if &opt_cim_dgn_ass. > 0 %then %do;	
						%let flt_dgn_cim_m_pal = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) or';					
						%let flt_dgn_cim_m_rel = '(%multi_like(var = T_MCOB.DGN_REL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) or';
						%let flt_dgn_cim_m_ass = '(%multi_like(var = T_MCOD.ASS_DGN, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%let flt_dgn_cim_p_pal = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%let flt_dgn_cim_s_pal = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%let flt_dgn_cim_s_ass = '(%multi_like(var = T_SSRD.DGN_COD, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) or';
							%let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADDMPA.DGN_ASS_MPA, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) or';
		 						   %let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADD.DGN_ASS, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';%end;
					%end;			
					%else %do;		
						%let flt_dgn_cim_m = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%let flt_dgn_cim_p = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%let flt_dgn_cim_s = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';%end;
					%end;
				%end;
			%end;
			%else %do;
				%if %length(&opt_cim_exc.) > 0 %then %do;
					%if &opt_cim_dgn_ass. > 0 %then %do;	
						%let flt_dgn_cim_m_pal = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';					
						%let flt_dgn_cim_m_rel = '(%multi_like(var = T_MCOB.DGN_REL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
						%let flt_dgn_cim_m_ass = '(%multi_like(var = T_MCOD.ASS_DGN, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_p_pal = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s_pal = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s_ass = '(%multi_like(var = T_SSRD.DGN_COD, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
							%let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADDMPA.DGN_ASS_MPA, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h_pal = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC)) or';
		 						   %let flt_dgn_cim_h_ass = '(%multi_like(var = T_HADD.DGN_ASS, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
					%end;			
					%else %do;		
						%let flt_dgn_cim_m = '(%multi_like(var = T_MCOB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_p = '(%multi_like(var = T_RIPRSA.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%let flt_dgn_cim_s = '(%multi_like(var = T_SSRB.MOR_PRP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
						%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
							%let flt_dgn_cim_h = '(%multi_like(var = T_HADDMPP.DGN_ASS_MPP, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
						%else %do; %let flt_dgn_cim_h = '(%multi_like(var = T_HADB.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';%end;
					%end;
				%end;
				%else %do;
					%let flt_dgn_cim_m_pal = 'T_MCOB.DGN_PAL is not NULL';
					%if &aaaa. = 2012 or &aaaa. = 2013 %then %do; 
						%let flt_dgn_cim_h_pal = 'T_HADDMPP.DGN_ASS_MPP is not NULL'; 
					%end;
					%else %do; 
						%let flt_dgn_cim_h_pal = 'T_HADB.DGN_PAL is not NULL'; 
					%end;
					%let flt_dgn_cim_p_pal = 'T_RIPRSA.DGN_PAL is not NULL';
					%let flt_dgn_cim_s_pal = 'T_SSRB.MOR_PRP is not NULL';

					%let flt_dgn_cim_m = 'T_MCOB.DGN_PAL is not NULL';
					%if &aaaa. = 2012 or &aaaa. = 2013 %then %do; 
						%let flt_dgn_cim_h = 'T_HADDMPP.DGN_ASS_MPP is not NULL'; 
					%end;
					%else %do; 
						%let flt_dgn_cim_h = 'T_HADB.DGN_PAL is not NULL'; 
					%end;
					%let flt_dgn_cim_p = 'T_RIPRSA.DGN_PAL is not NULL';
					%let flt_dgn_cim_s = 'T_SSRB.MOR_PRP is not NULL';
				%end;
			%end;
		%end;
	/* Selection des filtres MCO pour les resumes d'unite medicale */
		%if &opt_dpl_rum. = 1 %then %do;/* MCO RUM */
			%if &opt_cim_inc. > 0 %then %do;
				%if &opt_cim_exc. > 0 %then %do;
					%let flt_dgn_cim_m_rum = '(%multi_like(var = T_MCOUM.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC)) and (%multi_like(var = T_MCOUM.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
				%end;
				%else %do;
					%let flt_dgn_cim_m_rum = '(%multi_like(var = T_MCOUM.DGN_PAL, lst_cod = &opt_cim_inc., aim = INC, stp = PROC))';
				%end;
			%end;
			%else %do;
				%if &opt_cim_exc. > 0 %then %do;
					%let flt_dgn_cim_m_rum = '(%multi_like(var = T_MCOUM.DGN_PAL, lst_cod = &opt_cim_exc., aim = EXC, stp = PROC))';
				%end;
				%else %do;
					%let flt_dgn_cim_m_rum = 'T_MCOUM.DGN_PAL is not NULL';
				%end;
			%end;
		%end;
/* Fin du choix de la liste des CIM-10 */


/* ------------- */
/* --- Macro --- */
/* ------------- */
	%if (&opt_cim_inc. > 0 or &opt_cim_exc. > 0) or (&opt_cim_inc. = and &opt_cim_exc. = and &opt_pop. > 0) %then %do;

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
		%if &opt_dgn. = 1 %then %do;
/* On fait ici le choix de prendre les diagnostics associes */
			%if &opt_cim_dgn_ass. = 1 %then %do;
	/* Extraction selon le diagnostic principal et aussi associe/relie */
			/*  MCO  */
				%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. ne 1 %then %do;
					%if &aaaa. LE 2008 %then %do;
						/*%if T_MCOB.SEJ_NBJ = . %then  T_MCOB.SEJ_NBJ = 0;*/
						%let var_dtf = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)-T_MCOB.SEJ_NBJ*;
					%end;
					%else %do;
						%let var_dtd = *T_MCOC.EXE_SOI_DTD*;
						%let var_dtf = *T_MCOC.EXE_SOI_DTF*;
					%end;
				%checkDrop(&tab_out._&aaaa._MCO);
					proc sql;
					%connectora;
					EXECUTE(
					create table &tab_out._&aaaa._mco as
					select distinct T_MCOC.NIR_ANO_17,
							T_MCOB.ETA_NUM,
							T_MCOB.RSA_NUM,
							CASE WHEN T_MCOB.SEJ_NBJ IS NULL THEN 0 ELSE T_MCOB.SEJ_NBJ END AS SEJ_NBJ,
							%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
							%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
							T_MCOA.CDC_ACT,
							'MCO' as HSP_OGN,
							T_MCOB.DGN_PAL,
							T_MCOB.DGN_REL,
							T_MCOD.ASS_DGN 
					from %scan(&jnt_mco_pop., 1, "'")
							inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)
							/*inner*/ left join T_MCO&aa.D T_MCOD on (T_MCOC.ETA_NUM = T_MCOD.ETA_NUM and T_MCOC.RSA_NUM = T_MCOD.RSA_NUM)
							left join T_MCO&aa.A T_MCOA on (T_MCOC.ETA_NUM = T_MCOA.ETA_NUM and T_MCOC.RSA_NUM = T_MCOA.RSA_NUM)  			
					where (%scan(&flt_dgn_cim_m_pal., 1, "'") %scan(&flt_dgn_cim_m_rel., 1, "'") %scan(&flt_dgn_cim_m_ass., 1, "'")) and         
							(T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750010083', '750100091', '750100109', '750100125', '750100166', '750100208',
													'750100216', '750100232', '750100273', '750100299', '750801441', '750803447', '750803454', '910100015',
													'910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011',
													'930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
													'690783154', '690784137', '690784152', '690784178', '690787478', '830100558') 
						and not(T_MCOB.GRG_RET = '024')
						and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z') /*90%*/
						and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is NULL) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z')))))
					BY ORACLE;
					quit;

					proc sql;
						create table &lib_out..&tab_out._&aaaa._MCO as 
						select NIR_ANO_17, 
								ETA_NUM, 
								RSA_NUM, 
								datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
								datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
								CDC_ACT,
								HSP_OGN, 
								SEJ_NBJ, 
								DGN_PAL, 
								DGN_REL, 
								ASS_DGN
						from orauser.&tab_out._&aaaa._MCO;
					quit;
					%checkDrop(&tab_out._&aaaa._MCO);
				%end; /* Fermeture de la partie MCO  */

			/* HAD  */
				%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &aaaa. GE 2005 %then %do;
					%if &aaaa. LE 2008 %then %do;
						%let var_dtf = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)*;
						%let var_dtd = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)-T_HADB.SEJ_NBJ*;
					%end;
					%else %do;
						%let var_dtd = *T_HADC.EXE_SOI_DTD*;
						%let var_dtf = *T_HADC.EXE_SOI_DTF*;
					%end;

					%if &aaaa. LE 2009 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
											T_HADC.ETA_NUM_EPMSI as ETA_NUM,
											T_HADC.RHAD_NUM as RSA_NUM,
											%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
											%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
											'HAD' as HSP_OGN,
											CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
											T_HADB.DGN_PAL as DGN_PAL,
								 			T_HADB.DGN_PAL1 as ASS_DGN,
									 		T_HADB.DGN_PAL2 as ASS_DGN,
								 			T_HADB.DGN_PAL3 as ASS_DGN,
											T_HADB.DGN_PAL4 as ASS_DGN,
											T_HADB.DGN_PAL5 as ASS_DGN,
											T_HADB.DGN_PAL6 as ASS_DGN,
											T_HADB.DGN_PAL7 as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
								inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)			
							where %scan(&flt_dgn_cim_h_pal., 1, "'") and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;
					
						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;
						%checkDrop(&tab_out._&aaaa._HAD);
					%end;
					%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
											T_HADC.ETA_NUM_EPMSI as ETA_NUM,
											T_HADC.RHAD_NUM as RSA_NUM,
											%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
											%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
											CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
											'HAD' as HSP_OGN,
											T_HADDMPP.DGN_ASS_MPP as DGN_PAL,
								 			T_HADDMPA.DGN_ASS_MPA as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
									inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)
								 	inner join T_HAD&aa.DMPP T_HADDMPP on (T_HADC.ETA_NUM_EPMSI = T_HADDMPP.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADDMPP.RHAD_NUM)
								 	/*inner*/ left join T_HAD&aa.DMPA T_HADDMPA on (T_HADC.ETA_NUM_EPMSI = T_HADDMPA.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADDMPA.RHAD_NUM)			
							where (%scan(&flt_dgn_cim_h_pal., 1, "'") %scan(&flt_dgn_cim_h_ass., 1, "'")) and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;

						%checkDrop(&tab_out._&aaaa._HAD);
					%end;
					%if &aaaa. = 2010 or &aaaa. = 2011 or &aaaa. GE 2014 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
											T_HADC.ETA_NUM_EPMSI as ETA_NUM,
											T_HADC.RHAD_NUM as RSA_NUM,
											%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
											%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
											CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
											'HAD' as HSP_OGN,
											T_HADB.DGN_PAL,
								 			T_HADD.DGN_ASS as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
									inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)
									/*inner*/ left join T_HAD&aa.D T_HADD on (T_HADC.ETA_NUM_EPMSI = T_HADD.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADD.RHAD_NUM)			
							where (%scan(&flt_dgn_cim_h_pal., 1, "'") %scan(&flt_dgn_cim_h_ass., 1, "'")) and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;

						%checkDrop(&tab_out._&aaaa._HAD);
					%end;
				%end; /* Fermeture de la partie HAD */	

			/*  SSR  */
				%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) and &aaaa. GE 2005 %then %do;
					%if &aaaa. LE 2008 %then %do;
						%let var_dtf = *to_date('15-' || T_SSRC.SOR_MOI || '-' || T_SSRC.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_SSRC.SOR_MOI || '-' || T_SSRC.SOR_ANN)-T_SSRS.SEJ_NBJ*;
					%end;
					%else %do;
						%let var_dtd = *T_SSRC.EXE_SOI_DTD*;
						%let var_dtf = *T_SSRC.EXE_SOI_DTF*;
					%end;

					%if &aaaa. = 2005 or &aaaa. = 2006 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._SSR as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								'SSR' as HSP_OGN,
								T_SSRB.MOR_PRP as DGN_PAL,
					 			T_SSRB.ASS_DGN_1 as ASS_DGN,
						 		T_SSRB.ASS_DGN_2 as ASS_DGN,
					 			T_SSRB.ASS_DGN_3 as ASS_DGN,
								T_SSRB.ASS_DGN_4 as ASS_DGN,
								T_SSRB.ASS_DGN_5 as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 
						where %scan(&flt_dgn_cim_s_pal., 1, "'") or %scan(&flt_dgn_cim_s_ass., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;

						%checkDrop(&tab_out._&aaaa._SSR);
					%end;
					%if &aaaa. = 2007 or &aaaa. = 2008 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._SSR as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								'SSR' as HSP_OGN,
								T_SSRB.MOR_PRP as DGN_PAL,
					 			T_SSRB.ASS_DGN_1 as ASS_DGN,
						 		T_SSRB.ASS_DGN_2 as ASS_DGN,
					 			T_SSRB.ASS_DGN_3 as ASS_DGN,
								T_SSRB.ASS_DGN_4 as ASS_DGN,
								T_SSRB.ASS_DGN_5 as ASS_DGN,
								T_SSRB.ASS_DGN_6 as ASS_DGN,
						 		T_SSRB.ASS_DGN_7 as ASS_DGN,
					 			T_SSRB.ASS_DGN_8 as ASS_DGN,
								T_SSRB.ASS_DGN_9 as ASS_DGN,
					 			T_SSRB.ASS_DGN_10 as ASS_DGN,
								T_SSRB.ASS_DGN_11 as ASS_DGN,
						 		T_SSRB.ASS_DGN_12 as ASS_DGN,
					 			T_SSRB.ASS_DGN_13 as ASS_DGN,
								T_SSRB.ASS_DGN_14 as ASS_DGN,
								T_SSRB.ASS_DGN_15 as ASS_DGN,
								T_SSRB.ASS_DGN_16 as ASS_DGN,
						 		T_SSRB.ASS_DGN_17 as ASS_DGN,
					 			T_SSRB.ASS_DGN_18 as ASS_DGN,
								T_SSRB.ASS_DGN_19 as ASS_DGN,
								T_SSRB.ASS_DGN_20 as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 
						where %scan(&flt_dgn_cim_s_pal., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;

						%checkDrop(&tab_out._&aaaa._SSR);
					%end;
					%if &aaaa. GE 2009 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._SSR as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								'SSR' as HSP_OGN,
								T_SSRB.MOR_PRP as DGN_PAL,
								T_SSRD.DGN_COD as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							/*inner*/ left join T_SSR&aa.D T_SSRD on (T_SSRC.ETA_NUM = T_SSRD.ETA_NUM and T_SSRC.RHA_NUM = T_SSRD.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 	
						where %scan(&flt_dgn_cim_s_pal., 1, "'") or %scan(&flt_dgn_cim_s_ass., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 	
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;
						%checkDrop(&tab_out._&aaaa._SSR);
					%end;
				%end; /* Fermeture de la partie SSR */


				
			/*  PSY  */
				%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)) and &aaaa. GE 2007 %then %do;
					%if &aaaa. GE 2009 %then %do;
						%let var_dtd = 'T_RIPC.EXE_SOI_DTD';
						%let var_dtf = 'T_RIPC.EXE_SOI_DTF';
					%end;
					%else %do;
						%let var_dtf = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)-T_RIPS.SEJ_NBJ*;
					%end;
					%checkDrop(&tab_out._&aaaa._PSY);
					proc sql;
					%connectora;
					EXECUTE(
						create table &tab_out._&aaaa._PSY as
						select distinct T_RIPC.NIR_ANO_17,
								T_RIPC.ETA_NUM_EPMSI as ETA_NUM,
								T_RIPC.RIP_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "'") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "'") as EXE_SOI_DTF,
								'PSY' as HSP_OGN,
								T_RIPRSA.DGN_PAL,							
								T_RIPRSAD.ASS_DGN
						from %scan(&jnt_psy_pop., 1, "'") 		
							inner join T_RIP&aa.RSA T_RIPRSA on (T_RIPC.ETA_NUM_EPMSI = T_RIPRSA.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPRSA.RIP_NUM)
							/*inner*/ left join T_RIP&aa.RSAD T_RIPRSAD on (T_RIPC.ETA_NUM_EPMSI = T_RIPRSAD.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPRSAD.RIP_NUM)
						where %scan(&flt_dgn_cim_p_pal., 1, "'"))
					BY ORACLE;
					quit;

					proc sql;
						create table &lib_out..&tab_out._&aaaa._PSY as 
						select NIR_ANO_17, 
								ETA_NUM, 
								RSA_NUM, 
								datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
								datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
								HSP_OGN, 
								DGN_PAL, 
								ASS_DGN
						from orauser.&tab_out._&aaaa._PSY;
					quit;

					%checkDrop(&tab_out._&aaaa._PSY);

				%end; /* Fermeture de la partie PSY - Pas de diagnostics associes dans ces tables */
		%end; /* Fin de la partie - extraction des diagnostics principaux et associes selon la CIM-10 */


	/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

		%else %do; /* Permet d'extraire les hospitalisations uniquement selon la CIM-10 du diagnostic principal, avec extraits des diagnotics associes/relies */ 
				/*  MCO  */
				%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. ne 1 %then %do;
					%if &aaaa. LE 2008 %then %do;
						%let var_dtf = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)-T_MCOB.SEJ_NBJ*;
					%end;
					%else %do;
						%let var_dtd = *T_MCOC.EXE_SOI_DTD*;
						%let var_dtf = *T_MCOC.EXE_SOI_DTF*;
					%end;
					%checkDrop(&tab_out._&aaaa._MCO);
					proc sql;
					%connectora;
					EXECUTE(
					create table &tab_out._&aaaa._MCO as
					select distinct T_MCOC.NIR_ANO_17,
							T_MCOB.ETA_NUM,
							T_MCOB.RSA_NUM,
							%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
							%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
							CASE WHEN T_MCOB.SEJ_NBJ IS NULL THEN 0 ELSE T_MCOB.SEJ_NBJ END AS SEJ_NBJ,
							T_MCOA.CDC_ACT,
							'MCO' as HSP_OGN,
							T_MCOB.DGN_PAL,
							T_MCOB.DGN_REL,
							T_MCOD.ASS_DGN
					from %scan(&jnt_mco_pop., 1, "'")
							inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)
							/*inner*/ left join T_MCO&aa.D T_MCOD on (T_MCOC.ETA_NUM = T_MCOD.ETA_NUM and T_MCOC.RSA_NUM = T_MCOD.RSA_NUM)
							left join T_MCO&aa.A T_MCOA on (T_MCOC.ETA_NUM = T_MCOA.ETA_NUM and T_MCOC.RSA_NUM = T_MCOA.RSA_NUM)  		
					where %scan(&flt_dgn_cim_m., 1, "'") and         
							(T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
											    	'750100042', '750100075', '750010083', '750100091', '750100109', '750100125', '750100166', '750100208',
													'750100216', '750100232', '750100273', '750100299', '750801441', '750803447', '750803454', '910100015',
													'910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011',
													'930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
													'690783154', '690784137', '690784152', '690784178', '690787478', '830100558') 
						and not(T_MCOB.GRG_RET = '024')
						and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
						and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is null) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z')))))
					BY ORACLE;
					quit;
		

					proc sql;
						create table &lib_out..&tab_out._&aaaa._MCO as 
						select NIR_ANO_17, 
								ETA_NUM, 
								RSA_NUM, 
								datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
								datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
								CDC_ACT,
								HSP_OGN, 
								SEJ_NBJ, 
								DGN_PAL, 
								DGN_REL, 
								ASS_DGN
						from orauser.&tab_out._&aaaa._MCO;
					quit;
					%checkDrop(&tab_out._&aaaa._MCO);

				%end; /* Fermeture de la partie MCO */

				/* HAD */
				%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &aaaa. GE 2005	 %then %do;
					%if &aaaa. LE 2008 %then %do;
						%let var_dtf = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)*;
						%let var_dtd = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)-T_HADB.SEJ_NBJ*;
					%end;
					%else %do;
						%let var_dtd = *T_HADC.EXE_SOI_DTD*;
						%let var_dtf = *T_HADC.EXE_SOI_DTF*;
					%end;

					%if &aaaa. LE 2009 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
											T_HADC.ETA_NUM_EPMSI as ETA_NUM,
											T_HADC.RHAD_NUM as RSA_NUM,
											%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
											%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
											CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
											'HAD' as HSP_OGN,
											T_HADB.DGN_PAL as DGN_PAL,
								 			T_HADB.DGN_PAL1 as ASS_DGN,
									 		T_HADB.DGN_PAL2 as ASS_DGN,
								 			T_HADB.DGN_PAL3 as ASS_DGN,
											T_HADB.DGN_PAL4 as ASS_DGN,
											T_HADB.DGN_PAL5 as ASS_DGN,
											T_HADB.DGN_PAL6 as ASS_DGN,
											T_HADB.DGN_PAL7 as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
								inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)			
							where %scan(&flt_dgn_cim_h., 1, "'") and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;


						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;
						%checkDrop(&tab_out._&aaaa._HAD);

					%end;
					%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
								T_HADC.ETA_NUM_EPMSI as ETA_NUM,
								T_HADC.RHAD_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,											
								'HAD' as HSP_OGN,
								T_HADDMPP.DGN_ASS_MPP as DGN_PAL,
					 			T_HADDMPA.DGN_ASS_MPA as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
									inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)
								 	inner join T_HAD&aa.DMPP T_HADDMPP on (T_HADC.ETA_NUM_EPMSI = T_HADDMPP.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADDMPP.RHAD_NUM)
								 	/*inner*/ left join T_HAD&aa.DMPA T_HADDMPA on (T_HADC.ETA_NUM_EPMSI = T_HADDMPA.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADDMPA.RHAD_NUM)			
							where %scan(&flt_dgn_cim_h., 1, "'") and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;

						%checkDrop(&tab_out._&aaaa._HAD);
					%end;
					%if &aaaa. = 2010 or &aaaa. = 2011 or &aaaa. GE 2014 %then %do;
					%checkDrop(&tab_out._&aaaa._HAD);
						proc sql;
						%connectora;
						EXECUTE(
							create table &tab_out._&aaaa._had as
							select distinct T_HADC.NIR_ANO_17,
											T_HADC.ETA_NUM_EPMSI as ETA_NUM,
											T_HADC.RHAD_NUM as RSA_NUM,
											%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
											%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
											CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
											'HAD' as HSP_OGN,
											T_HADB.DGN_PAL,
								 			T_HADD.DGN_ASS as ASS_DGN
							from %scan(&jnt_had_pop., 1, "'")
									inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)
									left join T_HAD&aa.D T_HADD on (T_HADC.ETA_NUM_EPMSI = T_HADD.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADD.RHAD_NUM)			
							where %scan(&flt_dgn_cim_h., 1, "'") and
								  T_HADC.NIR_RET = '0' and
								  T_HADC.NAI_RET = '0' and
								  T_HADC.SEX_RET = '0' and
								  T_HADC.SEJ_RET = '0' and
								  T_HADC.FHO_RET = '0' and
								  T_HADC.PMS_RET = '0' and
								  T_HADC.DAT_RET = '0')
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._HAD as 
							select NIR_ANO_17, 
									ETA_NUM, RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._HAD;
						quit;

						%checkDrop(&tab_out._&aaaa._HAD);
					%end;
				%end; /* Fermeture de la partie HAD */	


			/*  SSR */
				%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) and &aaaa. GE 2005 %then %do;
					%if &aaaa. LE 2008 %then %do;
						%let var_dtf = *to_date('15-' || T_SSRB.SOR_MOI|| '-' || T_SSRB.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_SSRB.SOR_MOI || '-' || T_SSRB.SOR_ANN)-T_SSRS.SEJ_NBJ*;
					%end;

					%else %do;
						%let var_dtd = *T_SSRC.EXE_SOI_DTD*;
						%let var_dtf = *T_SSRC.EXE_SOI_DTF*;
					%end;

					%if &aaaa. = 2005 or &aaaa. = 2006 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._ssr as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								'SSR' as HSP_OGN,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								T_SSRB.MOR_PRP as DGN_PAL,
					 			T_SSRB.ASS_DGN_1 as ASS_DGN,
						 		T_SSRB.ASS_DGN_2 as ASS_DGN,
					 			T_SSRB.ASS_DGN_3 as ASS_DGN,
								T_SSRB.ASS_DGN_4 as ASS_DGN,
								T_SSRB.ASS_DGN_5 as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 
						where %scan(&flt_dgn_cim_s., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;

						%checkDrop(&tab_out._&aaaa._SSR);
					%end;

					%if &aaaa. = 2007 or &aaaa. = 2008 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._ssr as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								'SSR' as HSP_OGN,
								T_SSRB.MOR_PRP as DGN_PAL,
					 			T_SSRB.ASS_DGN_1 as ASS_DGN,
						 		T_SSRB.ASS_DGN_2 as ASS_DGN,
					 			T_SSRB.ASS_DGN_3 as ASS_DGN,
								T_SSRB.ASS_DGN_4 as ASS_DGN,
								T_SSRB.ASS_DGN_5 as ASS_DGN,
								T_SSRB.ASS_DGN_6 as ASS_DGN,
						 		T_SSRB.ASS_DGN_7 as ASS_DGN,
					 			T_SSRB.ASS_DGN_8 as ASS_DGN,
								T_SSRB.ASS_DGN_9 as ASS_DGN,
					 			T_SSRB.ASS_DGN_10 as ASS_DGN,
								T_SSRB.ASS_DGN_11 as ASS_DGN,
						 		T_SSRB.ASS_DGN_12 as ASS_DGN,
					 			T_SSRB.ASS_DGN_13 as ASS_DGN,
								T_SSRB.ASS_DGN_14 as ASS_DGN,
								T_SSRB.ASS_DGN_15 as ASS_DGN,
								T_SSRB.ASS_DGN_16 as ASS_DGN,
						 		T_SSRB.ASS_DGN_17 as ASS_DGN,
					 			T_SSRB.ASS_DGN_18 as ASS_DGN,
								T_SSRB.ASS_DGN_19 as ASS_DGN,
								T_SSRB.ASS_DGN_20 as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 
						where %scan(&flt_dgn_cim_s., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;
						%checkDrop(&tab_out._&aaaa._SSR);
					%end;

					%if &aaaa. GE 2009 %then %do;
					%checkDrop(&tab_out._&aaaa._SSR);
						proc sql;
						%connectora;
						EXECUTE(
						create table &tab_out._&aaaa._ssr as
						select distinct T_SSRC.NIR_ANO_17,
								T_SSRB.ETA_NUM,
								T_SSRB.RHA_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
								'SSR' as HSP_OGN,
								CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
								T_SSRB.MOR_PRP as DGN_PAL,
								T_SSRD.DGN_COD as ASS_DGN
						from %scan(&jnt_ssr_pop., 1, "'")
							inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
							/*inner*/ left join T_SSR&aa.D T_SSRD on (T_SSRC.ETA_NUM = T_SSRD.ETA_NUM and T_SSRC.RHA_NUM = T_SSRD.RHA_NUM)
							inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 	
						where %scan(&flt_dgn_cim_s., 1, "'"))
						BY ORACLE;
						quit;

						proc sql;
							create table &lib_out..&tab_out._&aaaa._SSR as 
							select NIR_ANO_17, 
									ETA_NUM, 
									RSA_NUM, 
									datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
									datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
									HSP_OGN, 
									SEJ_NBJ, 
									DGN_PAL, 
									ASS_DGN
							from orauser.&tab_out._&aaaa._SSR;
						quit;

						%checkDrop(&tab_out._&aaaa._SSR);
					%end;
				%end; /* Fermeture de la partie SSR */

	
			/*  PSY */
				%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)) and &aaaa. GE 2007 %then %do;
					%if &aaaa. GE 2009 %then %do;
						%let var_dtd = 'T_RIPC.EXE_SOI_DTD';
						%let var_dtf = 'T_RIPC.EXE_SOI_DTF';
					%end;
					%else %do;
						%let var_dtf = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)*;
						%let var_dtd = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)-T_RIPS.SEJ_NBJ*;
					%end;
					%checkDrop(&tab_out._&aaaa._PSY);
					proc sql;
					%connectora;
					EXECUTE(					
						create table &tab_out._&aaaa._PSY as
						select distinct T_RIPC.NIR_ANO_17,
								T_RIPC.ETA_NUM_EPMSI as ETA_NUM,
								T_RIPC.RIP_NUM as RSA_NUM,
								%scan(&var_dtd, 1, "'") as EXE_SOI_DTD,
								%scan(&var_dtf, 1, "'") as EXE_SOI_DTF,
								T_RIPRSA.DGN_PAL,
								T_RIPRSAD.ASS_DGN,
								'PSY' as HSP_OGN


						from %scan(&jnt_psy_pop., 1, "'") 		
							inner join T_RIP&aa.RSA T_RIPRSA on (T_RIPC.ETA_NUM_EPMSI = T_RIPRSA.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPRSA.RIP_NUM)
							/*inner*/ left join T_RIP&aa.RSAD T_RIPRSAD on (T_RIPC.ETA_NUM_EPMSI = T_RIPRSAD.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPRSAD.RIP_NUM)
						
						where %scan(&flt_dgn_cim_p., 1, "'"))
					BY ORACLE;
					quit;

					proc sql;
						create table &lib_out..&tab_out._&aaaa._PSY as 
						select NIR_ANO_17, 
								ETA_NUM, 
								RSA_NUM, 
								datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
								datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
								HSP_OGN, 
								DGN_PAL, 
								ASS_DGN
						from orauser.&tab_out._&aaaa._PSY;
					quit;

					%checkDrop(&tab_out._&aaaa._PSY);
				%end; /* Fermeture de la partie PSY - Pas de diagnostics associes dans ces tables */
		%end; /* Fin de la partie - Extraction selon la CIM-10 du diagnostic principal uniquement, mais extraction des codes associes/relies */
	%end; /* Fermeture de la partie des extractions avec diagnostics */



	/* On fait ici le choix de PAS prendre les diagnostics associes et relies */
	%if &opt_dgn. NE 1 %then %do;

	/*  MCO hors DP RUM */
		%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. ne 1 %then %do;
			%if &aaaa. LE 2008 %then %do;
				%let var_dtf = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)*;
				%let var_dtd = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)-T_MCOB.SEJ_NBJ*;
			%end;
			%else %do;
				%let var_dtd = *T_MCOC.EXE_SOI_DTD*;
				%let var_dtf = *T_MCOC.EXE_SOI_DTF*;
			%end;
			%checkDrop(&tab_out._&aaaa._MCO);
			proc sql;
			%connectora;
			EXECUTE(
			create table &tab_out._&aaaa._mco as
			select distinct T_MCOC.NIR_ANO_17,
					T_MCOB.ETA_NUM,
					T_MCOB.RSA_NUM,
					%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
					%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
					T_MCOA.CDC_ACT,
					'MCO' as HSP_OGN,
					CASE WHEN T_MCOB.SEJ_NBJ IS NULL THEN 0 ELSE T_MCOB.SEJ_NBJ END AS SEJ_NBJ,
					T_MCOB.DGN_PAL

			from %scan(&jnt_mco_pop., 1, "'")
				inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)
				left join T_MCO&aa.A T_MCOA on (T_MCOC.ETA_NUM = T_MCOA.ETA_NUM and T_MCOC.RSA_NUM = T_MCOA.RSA_NUM) 		
			where %scan(&flt_dgn_cim_m., 1, "'") and      
				(T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
									    	'750100042', '750100075', '750100109', '750100125', '750100166', '750100208', '750100216', '750100232',
									    	'750100273', '750100299', '750801441', '750803447', '750803454', '910100015', '910100023', '920100013',
									    	'920100021', '920100039', '920100047', '920100054', '920100062', '930100011', '930100037', '930100045',
									    	'940100027', '940100035', '940100043', '940100050', '940100068', '950100016', '690783154', '690784137',
									    	'690784152', '690784178', '690787478', '830100558') 
				and not(T_MCOB.GRG_RET = '024')
				and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
				and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is null) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z')))))
			BY ORACLE;
			quit;

			proc sql;
				create table &lib_out..&tab_out._&aaaa._MCO as 
				select NIR_ANO_17, 
						ETA_NUM, 
						RSA_NUM, 
						datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
						datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
						CDC_ACT,
						HSP_OGN, 
						SEJ_NBJ, 
						DGN_PAL
						from orauser.&tab_out._&aaaa._MCO;
			quit;
			%checkDrop(&tab_out._&aaaa._MCO);
		%end;
	/* Fermeture de la partie MCO hors DP RUM */

	/*  MCO UM */
		%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. = 1 %then %do;
			%if &aaaa. LE 2008 %then %do;
				%let var_dtf = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)*;
				%let var_dtd = *to_date('15-' || T_MCOB.SOR_MOI || '-' || T_MCOB.SOR_ANN)-T_MCOB.SEJ_NBJ*;
			%end;
			%else %do;
				%let var_dtd = *T_MCOC.EXE_SOI_DTD*;
				%let var_dtf = *T_MCOC.EXE_SOI_DTF*;
			%end;
			%checkDrop(&tab_out._&aaaa._MCO);
			proc sql;
			%connectora;
			EXECUTE(
			create table &tab_out._&aaaa._mcoum as
			select distinct T_MCOC.NIR_ANO_17,
					T_MCOB.ETA_NUM,
					T_MCOB.RSA_NUM,
					%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
					%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
					'MCO RUM' as HSP_OGN,
					CASE WHEN T_MCOB.SEJ_NBJ IS NULL THEN 0 ELSE T_MCOB.SEJ_NBJ END AS SEJ_NBJ,
					T_MCOUM.DGN_PAL,
					T_MCOUM.PAR_DUR_SEJ,
					T_MCOB.ENT_MOD,
					T_MCOB.SOR_MOD

			from %scan(&jnt_mco_pop., 1, "'")
				inner join T_MCO&aa.B T_MCOB on (T_MCOC.ETA_NUM = T_MCOB.ETA_NUM and T_MCOC.RSA_NUM = T_MCOB.RSA_NUM)
				left join T_MCO&aa.UM T_MCOUM on (T_MCOC.ETA_NUM = T_MCOUM.ETA_NUM and T_MCOC.RSA_NUM = T_MCOUM.RSA_NUM) 		
			where %scan(&flt_dgn_cim_m_rum., 1, "'") and      
				(T_MCOC.ETA_NUM not in ('130780521', '130783236', '130783293', '130784234', '130804297', '600100101', '750041543', '750100018',
										'750100042', '750100075', '750010083', '750100091', '750100109', '750100125', '750100166', '750100208',
										'750100216', '750100232', '750100273', '750100299', '750801441', '750803447', '750803454', '910100015',
										'910100023', '920100013', '920100021', '920100039', '920100047', '920100054', '920100062', '930100011',
										'930100037', '930100045', '940100027', '940100035', '940100043', '940100050', '940100068', '950100016',
										'690783154', '690784137', '690784152', '690784178', '690787478', '830100558') 
				and not(T_MCOB.GRG_RET = '024')
				and T_MCOB.GRG_GHM not in ('90H01Z', '90Z00Z', '90Z01Z', '90Z02Z', '90Z03Z')
				and ((T_MCOB.SEJ_TYP = 'A' or T_MCOB.SEJ_TYP is null) or (T_MCOB.SEJ_TYP = 'B' and T_MCOB.GRG_GHM not in ('28Z14Z', '28Z15Z', '28Z16Z'))))
				)
			BY ORACLE;
			quit;

			proc sql;
				create table &lib_out..&tab_out._&aaaa._MCO_UM as 
				select NIR_ANO_17, 
						ETA_NUM, 
						RSA_NUM, 
						datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
						datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
						HSP_OGN, 
						SEJ_NBJ, 
						DGN_PAL,
						PAR_DUR_SEJ,
						ENT_MOD, 
						SOR_MOD
						from orauser.&tab_out._&aaaa._MCOUM;
			quit;
			%checkDrop(&tab_out._&aaaa._MCOUM);
		%end;
	/* Fermeture de la partie MCO UM */

	/* HAD */
		%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &aaaa. GE 2005 %then %do;
			%if &aaaa. LE 2008 %then %do;
				%let var_dtf = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)*;
				%let var_dtd = *to_date('15-' || T_HADB.SOR_MOI_SSEQ || '-' || T_HADB.SOR_ANN_SSEQ)-T_HADB.SEJ_NBJ*;
			%end;
			%else %do;
				%let var_dtd = *T_HADC.EXE_SOI_DTD*;
				%let var_dtf = *T_HADC.EXE_SOI_DTF*;
			%end;

			%if &aaaa. = 2012 or &aaaa. = 2013 %then %do;
			%checkDrop(&tab_out._&aaaa._HAD);
				proc sql;
					%connectora;
					EXECUTE(
					create table &tab_out._&aaaa._had as
					select distinct T_HADC.NIR_ANO_17,
									T_HADC.ETA_NUM_EPMSI as ETA_NUM,
									T_HADC.RHAD_NUM as RSA_NUM,
									%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
									%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
									CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
									T_HADDMPP.DGN_ASS_MPP as DGN_PAL,
									'HAD' as HSP_OGN
					from %scan(&jnt_had_pop., 1, "'")
							inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)
							inner join T_HAD&aa.DMPP T_HADDMPP on (T_HADC.ETA_NUM_EPMSI = T_HADDMPP.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADDMPP.RHAD_NUM)			
					where %scan(&flt_dgn_cim_h., 1, "'") and
						  T_HADC.NIR_RET = '0' and
						  T_HADC.NAI_RET = '0' and
						  T_HADC.SEX_RET = '0' and
						  T_HADC.SEJ_RET = '0' and
						  T_HADC.FHO_RET = '0' and
						  T_HADC.PMS_RET = '0' and
						  T_HADC.DAT_RET = '0')
					BY ORACLE;
				quit;

				proc sql;
					create table &lib_out..&tab_out._&aaaa._HAD as 
					select NIR_ANO_17, 
							ETA_NUM, 
							RSA_NUM, 
							datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
							datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
							HSP_OGN, 
							SEJ_NBJ, 
							DGN_PAL
					from orauser.&tab_out._&aaaa._HAD;
				quit;

				%checkDrop(&tab_out._&aaaa._HAD);	
			%end;
			%else %do;
			%checkDrop(&tab_out._&aaaa._HAD);
				proc sql;
					%connectora;
					EXECUTE(
					create table &tab_out._&aaaa._had as
					select distinct T_HADC.NIR_ANO_17,
									T_HADC.ETA_NUM_EPMSI as ETA_NUM,
									T_HADC.RHAD_NUM as RSA_NUM,
									%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
									%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
									CASE WHEN T_HADB.SEJ_NBJ IS NULL THEN 0 ELSE T_HADB.SEJ_NBJ END AS SEJ_NBJ,
									'HAD' as HSP_OGN,
									T_HADB.DGN_PAL
					from %scan(&jnt_had_pop., 1, "'")
							inner join T_HAD&aa.B T_HADB on (T_HADC.ETA_NUM_EPMSI = T_HADB.ETA_NUM_EPMSI and T_HADC.RHAD_NUM = T_HADB.RHAD_NUM)		
					where %scan(&flt_dgn_cim_h., 1, "'") and
						  T_HADC.NIR_RET = '0' and
						  T_HADC.NAI_RET = '0' and
						  T_HADC.SEX_RET = '0' and
						  T_HADC.SEJ_RET = '0' and
						  T_HADC.FHO_RET = '0' and
						  T_HADC.PMS_RET = '0' and
						  T_HADC.DAT_RET = '0')
					BY ORACLE;
				quit;

				proc sql;
					create table &lib_out..&tab_out._&aaaa._HAD as 
					select NIR_ANO_17, 
							ETA_NUM, 
							RSA_NUM, 
							datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
							datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
							HSP_OGN, 
							SEJ_NBJ, 
							DGN_PAL
					from orauser.&tab_out._&aaaa._HAD;
				quit;
				%checkDrop(&tab_out._&aaaa._HAD);

			%end;								
		%end;  /* Fermeture de la partie HAD */

	/*  SSR */
		%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) and &aaaa. GE 2005 %then %do;
			%if &aaaa. LE 2008 %then %do;
			%let var_dtf = *to_date('15-' || T_SSRB.SOR_MOI || '-' || T_SSRB.SOR_ANN)*;
			%let var_dtd = *to_date('15-' || T_SSRB.SOR_MOI || '-' || T_SSRB.SOR_ANN)-T_SSRS.SEJ_NBJ*;
			%end;
			%else %do;
				%let var_dtd = *T_SSRC.EXE_SOI_DTD*;
				%let var_dtf = *T_SSRC.EXE_SOI_DTF*;
			%end;
			%checkDrop(&tab_out._&aaaa._SSR);
			proc sql;
					%connectora;
					EXECUTE(
			create table &tab_out._&aaaa._ssr as
			select distinct T_SSRC.NIR_ANO_17,
					T_SSRB.ETA_NUM,
					T_SSRB.RHA_NUM as RSA_NUM,
					%scan(&var_dtd, 1, "*") as EXE_SOI_DTD,
					%scan(&var_dtf, 1, "*") as EXE_SOI_DTF,
					'SSR' as HSP_OGN,
					CASE WHEN T_SSRS.SEJ_NBJ IS NULL THEN 0 ELSE T_SSRS.SEJ_NBJ END AS SEJ_NBJ,
					T_SSRB.MOR_PRP as DGN_PAL
			from %scan(&jnt_ssr_pop., 1, "'")
				inner join T_SSR&aa.B T_SSRB on (T_SSRC.ETA_NUM = T_SSRB.ETA_NUM and T_SSRC.RHA_NUM = T_SSRB.RHA_NUM)
				inner join T_SSR&aa.S T_SSRS on (T_SSRC.ETA_NUM = T_SSRS.ETA_NUM and T_SSRC.RHA_NUM = T_SSRS.RHA_NUM) 		
			/*where %scan(&flt_dgn_cim_s., 1, "'")*/)
			BY ORACLE;
			quit;


			proc sql;
				create table &lib_out..&tab_out._&aaaa._SSR as 
				select NIR_ANO_17,  
						ETA_NUM, RSA_NUM, 
						datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
						datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
						HSP_OGN, 
						SEJ_NBJ,
						DGN_PAL
				from orauser.&tab_out._&aaaa._SSR;
			quit;

			%checkDrop(&tab_out._&aaaa._SSR);
		%end; /* Fermeture de la partie SSR */


	/*  PSY */
		%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)) and &aaaa. GE 2007 %then %do;
			%if &aaaa. GE 2009 %then %do;
				%let var_dtd = 'T_RIPC.EXE_SOI_DTD';
				%let var_dtf = 'T_RIPC.EXE_SOI_DTF';
			%end;
			%else %do;
				%let var_dtf = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)*;
				%let var_dtd = *to_date('15-' || T_RIPC.SOR_MOI || '-' || T_RIPC.SOR_ANN)-T_RIPS.SEJ_NBJ*;
			%end;
			%checkDrop(&tab_out._&aaaa._PSY);
			proc sql;
					%connectora;
					EXECUTE(
			create table &tab_out._&aaaa._PSY as
			select distinct T_RIPC.NIR_ANO_17,
					T_RIPC.ETA_NUM_EPMSI as ETA_NUM,
					T_RIPC.RIP_NUM as RSA_NUM,
					%scan(&var_dtd, 1, "'") as EXE_SOI_DTD,
					%scan(&var_dtf, 1, "'") as EXE_SOI_DTF,
					'PSY' as HSP_OGN,
					T_RIPRSA.DGN_PAL
			from %scan(&jnt_psy_pop., 1, "'") 		
				inner join T_RIP&aa.RSA T_RIPRSA on (T_RIPC.ETA_NUM_EPMSI = T_RIPRSA.ETA_NUM_EPMSI and T_RIPC.RIP_NUM = T_RIPRSA.RIP_NUM)
			where %scan(&flt_dgn_cim_p., 1, "'"))
			BY ORACLE;
			quit;

			proc sql;
				create table &lib_out..&tab_out._&aaaa._PSY as 
				select NIR_ANO_17, 
						ETA_NUM, 
						RSA_NUM,
						datepart(EXE_SOI_DTD) as EXE_SOI_DTD format ddmmyy10., 
						datepart(EXE_SOI_DTF) as EXE_SOI_DTF format ddmmyy10., 
						HSP_OGN,
						DGN_PAL
				from orauser.&tab_out._&aaaa._PSY;
			quit;

			%checkDrop(&tab_out._&aaaa._PSY);

		%end; /* Fermeture de la partie PSY */
	%end; /* Fermeture de la partie des extractions sans diagnotics */
	%end;	/* Fermeture de la macro */
%end;/* Fin de la boucle des annees */

	/* Permet de concatener toutes les tables annuelles d'hospitalisation en une seule table */
	%if &opt_conc. = 1 %then %do;
		%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. ne 1 %then %do;
			data &lib_out..&tab_out._mco;
			set
			%do annee = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
				&lib_out..&tab_out._&annee._mco
			%end;;
			run;
		%end;

		%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. = 1 %then %do;
			data &lib_out..&tab_out._mco_um;
			set
			%do annee = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
				&lib_out..&tab_out._&annee._mco_um
			%end;;
			run;
		%end;

		%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &aaaa. GE 2005 %then %do;
			data &lib_out..&tab_out._had;
			set
			%do annee = %sysfunc(max(&ann_deb., 2007)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
				&lib_out..&tab_out._&annee._had
			%end;;
			run;
		%end;

		%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) and &aaaa. GE 2005 %then %do;
			data &lib_out..&tab_out._ssr;
			set
			%do annee = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
				&lib_out..&tab_out._&annee._ssr
			%end;;
			run;
		%end;
	
		%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) and &aaaa. GE 2007 %then %do;
			data &lib_out..&tab_out._psy;
			set
			%do annee = %sysfunc(max(&ann_deb., 2007)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
				&lib_out..&tab_out._&annee._psy
			%end;;
			run;
		%end;
	%end; /* Fin de la boucle permettant la concatenation des tables intermediaires */


		/* Permet de concatener toutes les tables H/M/P/S en une seule table */
	%if &opt_conc_ogn. = 1 %then %do;
		data &lib_out..&tab_out;
		set
			%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &opt_dpl_rum. ne 1 %then %do;
				&lib_out..&tab_out._mco
			%end;

			%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) %then %do;
				&lib_out..&tab_out._had
			%end;

			%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) %then %do;
				&lib_out..&tab_out._ssr
			%end;
		
			%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) %then %do;
				&lib_out..&tab_out._psy
			%end;;
		run;
	%end; /* Fin de la boucle permettant la concatenation des tables H/M/S/P */


	/* Permet de supprimer les tables annuelles/intermediaires d'hospitalisation */
	%if &opt_suppr. = 1 %then %do;
		%do aaaa = %sysfunc(max(&ann_deb., 2005)) %to %sysfunc(min(&ann_fin., &ann_act. - 1));
			%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. ne 1 %then %do;
				proc delete data= &lib_out..&tab_out._&aaaa._mco;run;%end;
			%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &aaaa. GE 2005 and &opt_dpl_rum. = 1  %then %do;
				proc delete data= &lib_out..&tab_out._&aaaa._mco_um;run;%end;
			%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &aaaa. GE 2005 %then %do;
				proc delete data= &lib_out..&tab_out._&aaaa._had;run;%end;
			%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) and &aaaa. GE 2005 %then %do;
				proc delete data= &lib_out..&tab_out._&aaaa._ssr;run;%end;
			%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) and &aaaa. GE 2007 %then %do;
				proc delete data= &lib_out..&tab_out._&aaaa._psy;run;%end;
		%end;
	%end; /* Fin de la boucle permettant la suppression des tables intermediaires */

		/* Permet de supprimer les tables H/M/P/S */
	%if &opt_suppr_ogn. = 1 %then %do;
		%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and &opt_dpl_rum. ne 1 %then %do;
				proc delete data= &lib_out..&tab_out._mco;run;%end;
		%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) %then %do;
			proc delete data= &lib_out..&tab_out._had;run;%end;
		%if (%index(&mod_hsp.,S)>0 or %index(&mod_hsp.,s)>0) %then %do;
			proc delete data= &lib_out..&tab_out._ssr;run;%end;
		%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) %then %do;
			proc delete data= &lib_out..&tab_out._psy;run;%end;
	%end; /* Fin de la boucle permettant la suppression des tables H/M/P/S */

	/* Messages d'informations sur les donnees extraites */
	%if &opt_cim_inc. = and &opt_cim_exc. = %then %do;
		%put WARNING: Aucun des 2 parametres indiquant les codes CIM-10 a extraire n ont ete renseignes. Vous avez extrait la totalite des hospitalisations concernees par les autres parametres;
	%end;
	%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &ann_deb. LE 2007 %then %do;
		%put WARNING: Aucune donnee sur les HAD n a ete extraite avant 2007. Donnees disponibles pour les 3 regimes (RG, RA, RSI) et les 10 SLM, depuis 2007.;
	%end;
	%if (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0) and &ann_deb. GE 2007 %then %do;
		%put WARNING: Les donnees liees aux hospitalisations HAD concernent les 3 regimes (RG, RA, RSI) et les 10 SLM (Depuis 2007).;
	%end;
	%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and (&ann_deb. LE 2009 and &ann_fin. GE 2005) %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent uniquement le regime general (De 2005 a 2009 inclus);
	%end;
	%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and (&ann_deb. LE 2015 and &ann_fin. GE 2010) %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent les 3 regimes (RG, RA, RSI), de 2010 a 2015 inclus.;
	%end;
	%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and (&ann_deb. LE &ann_act. and &ann_fin. GE 2016) %then %do;
		%put WARNING: Les donnees liees aux hospitalisations MCO concernent les 3 regimes (RG, RA, RSI) et les 10 SLM (Depuis Septembre 2016).;
	%end;
	%if (%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) and (&ann_deb. LE 2008 or &ann_fin. LE 2008) %then %do;
		%put WARNING: Les dates d hospitalisations MCO ont ete approximees. Ne disposant que du mois et annee de sortie, nous avons consid�ere une entree au 1er du mois, et une duree de sejour egale a la valeur SEJ_NBJ.;
	%end;
	%if ((%index(&mod_hsp.,M)>0 or %index(&mod_hsp.,m)>0) or (%index(&mod_hsp.,H)>0 or %index(&mod_hsp.,h)>0)) and (&ann_deb. LE 2007 or &ann_fin. LE 2005) %then %do;
		%put WARNING: Les donnees HAD n existent qu a partir de 2007. Aucune donnee n a donc a extraite avant cette annee. Les hospitalisations MCO sont uniquement proposees.;
	%end;
	%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) and &opt_dgn. = 1 %then %do;
		%put WARNING: Il n existe pas de diagnostics associes dans l extraction des donnees psychiatriques.;
	%end;
	%if (%index(&mod_hsp.,P)>0 or %index(&mod_hsp.,p)>0) and &ann_deb. LE 2008 %then %do;
		%put WARNING: En raison d un manque d informations sur la duree du sejour, les hospitalisations psychiatriques n ont pas ete extraites en 2007 et 2008.;
	%end; /* Fin des messages d'informations */

	%Stop_macro:
	/* Fin du programme */
%mend;
