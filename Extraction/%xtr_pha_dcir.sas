/************************************************************************************************
 *	Macro   : xtr_pha_dcir																		*
 *	*********************************************************************************************
 *	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*
 *	*********************************************************************************************
 *	Mises a jour : (le cas echeant : preciser date, auteur, et objet de la MAJ)					*
 *		30/10/2018 - MA : boucle sur le mois de flux combine a l instruction UNION pour gain 	*
 *							de temps pour requete dans DCIR (parallelise dans ORACLE)			*
 *							+ ajout option macro opt_sup_tpr (voir ci-dessous)					*
 *							+ ajout option SAS dbidirectexec et dbsliceparm						*
 *		 2/11/2018 - MA : ajout de l option opt_flt_rg pour filter sur les remboursements RG	*
 *		23/11/2018 - MA : ajout de l option opt_xtr_dos (voir ci-dessous) 						*
 *							+ correction d'erreurs lorsqu aucun code n est specifie 			*
 *							+ MAJ suite modification %multi_like_pha							*
 *	    18/07/2019 - MA :	suppression de l option opt_flt_rg et la variable BEN_RNG_GEM		*
 *							(devenu inutile suite creation referentiel pop.)					*
 *							+ ajout du parametre ann_arc pour identifier les tables archivees   *
 *							dans DCIR															*
 *							+ ajout de l option opt_xtr_cle pour extraire les cle de jointure	*
 *							+ ajout de l option opt_xtr_lib pour extraire les libelles 			*
 *							+ ajout d une macro %multi_like dediee appelee %multi_like_pha		*
 *		20/05/2020 - CM : Prise en charge du filtre FLX_DIS_DTD devenu obligatoire.				*
 *							N.B : N est pris en charge que l annee N et N+1, il peut avoir		*
 *							des remboursements a N+2, N+3 (tres rare)							*
 *							+ mise en conformite avec les nouvelles regles du portail			*
 *							+ utilisation de la macro %J9K de l ANSM pour les jointures			*
 *		10/09/2020 - EH : Decoupage des filtres FLX_DIS_DTD par mois							*
 *		03/11/2020 - MP : Modification variable CIP pour les annees 2006 et 2007 				*
 *		12/04/2021 - PY + EH : Passage sous Oracle												*
 *		29/09/2021 - PY : Recriture boucle par annee											*
 *		21/07/2022 - CM : Modification des variables ATC_C07 en ATC_CLA, L07 en LIB,			*
 *							et CND_TOP en PHA_GRD_CND	+ Modification des extractions des		*
 *							donnees prescripteurs et executant avec les options opt_xtr_psp		*
 *							et opt_xtr_pse														*
 * 		07/11/2022 - PYD : D�finition automatique ann�e archivage donn�es prestation			*
 *	*********************************************************************************************
 *	Description : Cette macro permet d extraire, dans les donnees du DCIR et durant une 		*
 *                 periode de temps donnee, les medicaments delivres identifies a partir des	*
 *                 codes ATC ou CIP.															*
 *				  Si aucun code (ATC, CIP) n est renseigne, la macro extrait toutes les 		*
 *                 delivrances de medicament sur la periode donnee. 							*
 *	*********************************************************************************************
 *	Parametres :																				*
 *		lib_prj 	: nom de la bibliotheque ou sera rangee la table de sortie					*
 *		tab_out 	: nom de la table de sortie													*
 *		dte_dbt_prs : date de debut de la periode d'extraction (format DD/MM/YYYY)				*
 *		dte_fin_prs : date de fin de la periode d'extraction (format DD/MM/YYYY)				*
 *		opt_pop 	: optionnel : table de population a "jointer". Doit �tre de la forme		*
 *					NomTable, et stock� dans ORAUSER ou la librairie d�finie par LIB_PRJ 		*
 *		opt_ref 	: optionnel : table de referentiel des medicaments a "jointer". Doit �tre   *
 *                  de la forme NomLibrairie.NomTable											*
 *		opt_atc_inc : optionnel : liste des codes ATC des medicaments a extraire				*
 *		opt_atc_exc : optionnel : liste des codes ATC des medicaments a ne pas extraire			*
 *		opt_cip 	: optionnel : liste des codes CIP a extraire (7 caracteres)					*	
 *		opt_xtr_lib	: optionnel : extraire les donnees des libelles (valeur : 1-oui)			*
 *		opt_xtr_dos	: optionnel : extraire les donnees des doses/unites (valeur : 1-oui)		*
 *		opt_xtr_psp	: optionnel : extraire les donnees des prescripteurs (valeur : 1-oui)		*
 *		opt_xtr_pse	: optionnel : extraire les donnees des executants (valeur : 1-oui)			*
 *		opt_xtr_cle	: optionnel : extraire les donnees des cles de jointure (valeur : 1-oui)	*
 *	*********************************************************************************************
 *	Remarques :																					*
 *		Pour extraire toutes les donnees de medicaments (ville), garder les parametres suivants *
 *		non renseignes : opt_atc_in, opt_atc_exc et opt_cip										*
 *	*********************************************************************************************
 *	Exemple :																					*
 *		%xtr_pha_dcir(																			*
 *			lib_prj 	= WORK,																	*
 *			tab_out 	= TEST_PHA,																*
 *			dte_dbt_prs	= 01/05/2009,															*
 *			dte_fin_prs = 31/12/2013,															*
 *			opt_pop 	= POP_DT2,																*
 *			opt_ref 	= SASDATA1.PHA_REF,														*
 *			opt_atc_inc = A10BH A10BD07 A10BD08,												*
 *			opt_atc_exc = A10BH03,																*
 *			opt_cip 	= 3400949121175,														*			
 *			opt_xtr_lib = 1,																	*
 *			opt_xtr_dos = 1,																	*
 *			opt_xtr_psp = 1,																	*
 *			opt_xtr_pse = 1,																	*
 *			opt_xtr_cle = 1																		*
 *		);																						*
 ************************************************************************************************/
options nosource nonotes nosymbolgen;
option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;


%macro xtr_pha_dcir(lib_prj, tab_out, dte_dbt_prs, dte_fin_prs, opt_pop, opt_ref, opt_atc_inc, opt_atc_exc, 
			opt_cip, opt_xtr_lib, opt_xtr_dos, opt_xtr_psp, opt_xtr_pse, opt_xtr_cle);

/*Macro suppression tables temporaires*/
%macro checkDrop(myTable);
	%if %SYSFUNC(exist(orauser.&myTable)) %then
		%do;

			proc sql;
				%connectora;
				EXECUTE(
					drop table &myTable
						)
					BY ORACLE;
		%end;
	%else
%mend;

/* Definition automatique annee archivage des donnees de prestation */
	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let aa = 2006;
	%let exist = 1;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(oravue.er_prs_f_&aa.))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_arc = %eval(&aa.-1);

/* Copie de la table population si existante */
	%if %length(&opt_pop.) > 0 %then %do;
		/* Copie table population princeps vers orauser*/
		%if %sysfunc(exist(orauser.&opt_pop.))=0 %then%do;
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
						as select * from &lib_prj..&opt_pop.;
				quit;
		%end;
		/* Copie table population princeps vers orauser avec delete initial */
		%if %sysfunc(exist(orauser.&opt_pop.)) %then %do;
			%checkDrop(&opt_pop.);
			proc sql;
				create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
					as select * from &lib_prj..&opt_pop.;
			quit;
		%end;
		/* Arret si la table renseignee n'existe pas */
		%if %sysfunc(exist(orauser.&opt_pop.))=0 and %sysfunc(exist(&lib_prj..&opt_pop.))=0 %then %do;
			%put ERROR: La table Population renseignee est absente de la librairie ou de ORAUSER. Verifiez le nom.;
			%goto Stop_macro;
		%end;

		%let jnt_prs_pop = '&opt_pop. t_pop inner join &T_PRS. t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA';
	%end;
	%else %do;
		%let jnt_prs_pop = '&T_PRS. t_prs';
	%end;

	
/* Macro ANSM pour jointure des 9 tables avec cle primaire */
%macro J9K(G,D,HELP=NO);
	/***
	Jointure sur les 9 cles entre 2 tables de DCIR (DCT exclus)
	"ne vous demandez pas ce que votre jointure peut faire pour vous,
	mais demandez-vous ce que vous voulez faire avec votre jointure" ;-)
	Source profil 69 : &Fichiers/ANSM/Macros/j9k.sas
	***/
	%if %upcase(&HELP)=YES %then
		%do;

			data _null_;
				put
					'Macro J9K: Jointure sur les 9 cles entre 2 tables de DCIR (DCT exclus)      '/
					"- G      : Alias de la table de gauche                                      "/
					"- D      : Alias de la table de droite                                      "/
					"- HELP   : Pour avoir cette aide, par defaut : NO                           "/
					"           L'aide neutralise l'execution                                    "/
					"Exemple d'utilisation : a introduire dans un PROC SQL                       "/
					'  select liste_variables_groupe, liste_variables_agregees                   '/
					'  from ORAVUE.ER_PRS_F as PRS inner join ORAVUE.ER_ETE_F as ETE             '/
					'  on %J9K(PRS,ETE)                                                          '/
					'  where filtre_fonctiontionnel                                              '/
					'  group by liste_variables_groupe                                           '/
					'  ;                                                                         '/
					'Exemple d''utilisation : a introduire dans un PROC SQL + %CONNECTORA        '/
					'  select liste_variables_groupe, liste_variables_agregees                   '/
					'  from ER_PRS_F PRS left join ER_ETE_F ETE                                  '/
					'  on %J9K(PRS,ETE)                                                          '/
					'  where filtre_fonctiontionnel                                              '/
					'  group by liste_variables_groupe                                           '/
					'  ;                                                                         '/
				;
			run;

			%return;
		%end;

	&G..DCT_ORD_NUM=&D..DCT_ORD_NUM AND &G..FLX_DIS_DTD=&D..FLX_DIS_DTD AND &G..FLX_EMT_ORD=&D..FLX_EMT_ORD AND
		&G..FLX_EMT_NUM=&D..FLX_EMT_NUM AND &G..FLX_EMT_TYP=&D..FLX_EMT_TYP AND &G..FLX_TRT_DTD=&D..FLX_TRT_DTD AND
		&G..ORG_CLE_NUM=&D..ORG_CLE_NUM AND &G..PRS_ORD_NUM=&D..PRS_ORD_NUM AND &G..REM_TYP_AFF=&D..REM_TYP_AFF
%mend J9K;

/* Macro necessaire pour extraction des codes ATC/CIP */
%macro multi_like_pha(var, lst_cod, aim, type);
	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);
		%let val = %qscan(&lst_cod., &i., %str( ));

		%if &aim. = INC %then %do;
			%if %superq(val) > 0 %then %do;
				%cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
			%end;

			%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
			%let ope = or;
		%end;

		%if &aim. = EXC %then %do;
			%if %superq(val) > 0 %then %do;
				%cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
			%end;

			%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
			%let ope = and;
		%end;

		%let i = %eval(&i. + 1);
	%end;
%mend;

/* Macro necessaire pour extraction des codes CIP a 7 caracteres */
%macro multi_like_pha_7(var, lst_cod, aim, type);
	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);
		%let val = %qscan(&lst_cod., &i., %str( ));

		%if &aim. = INC %then %do;
			%if %superq(val) > 0 %then %do;
				%cmpres(%str(&ope. %upcase(&var.) like %'%upcase(substr(&val.,6,7))%%%'))
			%end;

			%put %str(&ope. %upcase(&var.) like %'%upcase(substr(%superq(val),6,7))%%%');
			%let ope = or;
		%end;

		%if &aim. = EXC %then %do;
			%if %superq(val) > 0 %then %do;
				%cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(substr(&val.,6,7))%%%'))
			%end;

			%put %str(&ope. %upcase(&var.) not like %'%upcase(substr(%superq(val),6,7))%%%');
			%let ope = and;
		%end;

		%let i = %eval(&i. + 1);
	%end;
%mend;


/* Filtres ATC et CIP appliques sur une sous-table issue de IR_PHA_R */
	%if %length(&opt_atc_inc.) > 0 %then %do;
		%if %length(&opt_atc_exc.) > 0 %then %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%let flt_med = '((%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC)
									or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC)
									or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))
								and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';		
			%end;
			%else %do;
				%let flt_med = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC)
								and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
			%end;
		%end;
		%else %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%let flt_med = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC)
									or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC)
									or %multi_like_pha(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))';
			%end;
			%else %do;
				%let flt_med = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC))';
			%end;
		%end;
	%end;
	%else %do;
		%if %length(&opt_atc_exc.) > 0 %then %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%let flt_med = '((%multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)
									or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC))
								and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
			%end;
			%else %do;
				%let flt_med = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
			%end;
		%end;
		%else %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%let flt_med = '(%multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)
									or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC))';
			%end;
			%else %do;
				%let flt_med = 't_ref.PHA_ATC_CLA is not null';
			%end;
		%end;
	%end;

* Creation du referentiel medicaments si non existant;
%if &opt_ref. = ORAUSER.IR_PHA_R_EXTRACT %then %do;
	%put ERROR: La table Referentiel Medicaments que vous avez renseign� doit porter un nom different de ORAUSER.IR_PHA_R_EXTRACT;
	%goto Stop_macro;
%end;
%if %length(&opt_ref.) ne 0 and %sysfunc(exist(&opt_ref.))=0 %then %do;
	%put ERROR: La table renseignee portant sur le referentiel Medicaments est absente de cette libraire. Verifiez le nom;
	%goto Stop_macro;
%end;

/* Verification de la table du r�f�rentiel M�dicament */
	%if %length(&opt_ref.) > 0 %then %do;
		%checkDrop(IR_PHA_R_EXTRACT);
		%let lib_ref = %scan(&opt_ref.,1,'.');
		%let dsid = %sysfunc(open(%str(&opt_ref.)));

		%put &lib_ref.;
		/* Initialisation de la checklist des variables necessaires pour la jointure */
		%if &dsid. gt 0 %then %do;

			%let chk_atc=%sysfunc(varnum(&dsid,PHA_ATC_CLA));
			%let chk_cip=%sysfunc(varnum(&dsid,PHA_CIP_C13));
			
			%let chk_lib=%sysfunc(varnum(&dsid,PHA_ATC_LIB));
			%let chk_nom=%sysfunc(varnum(&dsid,PHA_MED_NOM));

			%let chk_nbr=%sysfunc(varnum(&dsid,PHA_UPC_NBR));
			%let chk_dos=%sysfunc(varnum(&dsid,PHA_SUB_DOS));
			%let chk_uni=%sysfunc(varnum(&dsid,PHA_DOS_UNI));

			%if &chk_atc. = 0 or &chk_cip. = 0 %then %do;
				%put ERROR: La table Referentiel Medicaments renseignee doit contenir les variables avec les noms PHA_ATC_CLA et PHA_CIP_C13;
				%goto Stop_macro;
			%end;
			%if &opt_xtr_lib. = 1 and (&chk_lib. = 0 or &chk_nom. = 0) %then %do;
				%put ERROR: La table Referentiel Medicaments renseignee doit contenir les variables avec les noms PHA_ATC_LIB et PHA_MED_NOM;
				%goto Stop_macro;
			%end;
			%if &opt_xtr_dos. = 1 and (&chk_nbr. = 0 or &chk_dos. = 0 or &chk_uni. = 0) %then %do;
				%put ERROR: La table Referentiel Medicaments renseignee doit contenir les variables avec les noms PHA_UPC_NBR, PHA_SUB_DOS et PHA_DOS_UNI;
				%goto Stop_macro;
			%end;
			/* Recuperation de la table Referentiel renseignee avec copie dans la partie ORAUSER pour accelerer les requ�tes */
			%if (&chk_atc. gt 0 and &chk_cip. gt 0 and &opt_xtr_lib. ne 1 and &opt_xtr_dos. ne 1) or
				(&chk_atc. gt 0 and &chk_cip. gt 0 and &opt_xtr_lib. = 1 and &chk_lib. gt 0 and &chk_nom. gt 0 and &opt_xtr_dos. ne 1) or
				(&chk_atc. gt 0 and &chk_cip. gt 0 and &opt_xtr_lib. ne 1 and &opt_xtr_dos. = 1 and &chk_nbr. gt 0 and &chk_dos. gt 0 and &chk_uni. gt 0) or
				(&chk_atc. gt 0 and &chk_cip. gt 0 and &opt_xtr_lib. = 1 and &chk_lib. gt 0 and &chk_nom. gt 0 and &opt_xtr_dos. = 1 and &chk_nbr. gt 0 and &chk_dos. gt 0 and &chk_uni. gt 0) %then %do;
				
				/* Duplication de la table de librairie dans ORAUSER, avec filtre m�dicaments */
				%if &lib_ref. = ORAUSER %then %do;
					proc sql;
					%connectora;
					EXECUTE(
						create table IR_PHA_R_EXTRACT as
						select *
						from
						&opt_ref. t_ref
						where %scan(&flt_med., 1,"'")
					)
					BY ORACLE;
					quit;
				%end;
				%if &lib_ref. ne ORAUSER %then %do;	
					%checkDrop(ref_temp_ir_pha);

					data orauser.ref_temp_ir_pha;
					set &opt_ref.;
					run;	

					proc sql;
					%connectora;
					EXECUTE(
						create table IR_PHA_R_EXTRACT as
						select *
						from
						ref_temp_ir_pha t_ref
						where %scan(&flt_med., 1,"'")
					)
					BY ORACLE;
					quit;

					%checkDrop(ref_temp_ir_pha);
				%end;
		
				%let opt_ref = IR_PHA_R_EXTRACT;
			%end;
			%let rc=%sysfunc(close(&dsid));
		%end;
	%end;

	/* Si aucun referentiel n'existe, une copie de IR_PHA_R est faite */
	%if %length(&opt_ref.) = 0 %then %do;
		%checkDrop(IR_PHA_R_EXTRACT);
		proc sql;
		%connectora;
		EXECUTE(
			create table IR_PHA_R_EXTRACT as
			select *
			from
			ir_pha_r t_ref
			where %scan(&flt_med., 1,"'")
		)
		BY ORACLE;
		quit;

		%let opt_ref = IR_PHA_R_EXTRACT;
	%end;


/******************************************************/
/* 2 -           Medicaments de ville                 */
/******************************************************/

%checkDrop(&tab_out.);
%let ann_deb = %sysfunc(substr(&dte_dbt_prs.,7,4));
%let ann_fin = %sysfunc(substr(&dte_fin_prs.,7,4));
%let dte_dbt_prs = %sysfunc(compress(&dte_dbt_prs., '/'));
%let dte_fin_prs = %sysfunc(compress(&dte_fin_prs., '/'));


%do aaaa = %sysfunc(max(&ann_deb., 2006)) %to &ann_fin.;

	%if &aaaa. <= &ann_arc. %then %do;
		%let limit = 11;
	%end;
	%else %do;
		%let date_fin=%sysfunc(putn(%sysfunc(INPUTN(%sysfunc(compress(&dte_fin_prs., '/')), ddmmyy8.)), date9.));
		%let date_auj=%sysfunc(putn(%sysfunc(INPUTN(%sysfunc(today(), ddmmyy8.), ddmmyy8.)), date9.)); 

		%let limit=%sysfunc(min(38,%sysfunc(intck(MONTH,"&date_fin"d,"&date_auj"d))));
	%end;

	/*Definir filtre sur dates execution de la prestation selon que les parametres date_dbt_prestation et date_fin_prestation
	sont renseignes ou non*/
	%if &aaaa. <= &ann_arc. %then %do;
		%let T_PRS = ER_PRS_F_&aaaa.;
		%let T_PHA = ER_PHA_F_&aaaa.;
	%end;
	%else %do;
		%let T_PRS = ER_PRS_F;
		%let T_PHA = ER_PHA_F;
	%end;

	/*2-1- Filtres sur les medicaments*/
	%if %length(&opt_atc_inc.) > 0 %then %do;
		%if %length(&opt_atc_exc.) > 0 %then %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%if &aaaa. = 2006 or &aaaa. = 2007 %then %do;
					%let flt_atc_cip = '((%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC) or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)) and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
				%end;
				%else %do;
					%let flt_atc_cip = '((%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC) or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC) or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)) and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
				%end;
			%end;
			%else %do;
				%let flt_atc_cip = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC) and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
			%end;
		%end;
		%else %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%if &aaaa. = 2006 or &aaaa. = 2007 %then %do;
					%let flt_atc_cip = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC) or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))';
				%end;
				%else %do;
					%let flt_atc_cip = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC) or %multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC) or %multi_like_pha(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))';
				%end;
			%end;
			%else %do;
				%let flt_atc_cip = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_inc., aim = INC))';
			%end;
		%end;
	%end;
	%else %do;
		%if %length(&opt_atc_exc.) > 0 %then %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%if &aaaa. = 2006 or &aaaa. = 2007 %then %do;
					%let flt_atc_cip = '((%multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)) and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
				%end;
				%else %do;
					%let flt_atc_cip = '((%multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC) or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC)) and %multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
				%end;
			%end;
			%else %do;
				%let flt_atc_cip = '(%multi_like_pha(var = t_ref.PHA_ATC_CLA, lst_cod = &opt_atc_exc., aim = EXC))';
			%end;
		%end;
		%else %do;
			%if %length(&opt_cip.) > 0 %then %do;
				%if &aaaa. = 2006 or &aaaa. = 2007 %then %do;
					%let flt_atc_cip = '(%multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))';
				%end;
				%else %do;
					%let flt_atc_cip = '(%multi_like_pha(var = to_char(t_ref.PHA_CIP_C13), lst_cod = &opt_cip., aim = INC) or %multi_like_pha_7(var = to_char(t_ref.PHA_PRS_IDE), lst_cod = &opt_cip., aim = INC))';
				%end;
			%end;
			%else %do;
				%let flt_atc_cip = 't_ref.PHA_ATC_CLA is not null';
			%end;
		%end;
	%end;


	/*2-2- Extractions des medicaments de ville*/
	/*2-2-1- Annees hors 2006 et 2007*/
	%if &aaaa. ne 2006 and &aaaa. ne 2007 %then %do;
		/*annee de flux N + 27 mois suivants*/
		%do i = 0 %to &limit.;
			%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
			%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

			proc sql;
				%connectora;
				EXECUTE(
					%if %sysfunc(exist(orauser.&tab_out.))=0 %then %do;
						create table &tab_out. as
					%end;
					%else %do;
						insert into &tab_out.
					%end;

					(select t_prs.BEN_NIR_PSA, t_prs.EXE_SOI_DTD, t_ref.PHA_ATC_CLA, t_ref.PHA_CIP_C13,
					%if &opt_xtr_lib. = 1 %then %do;
						t_ref.PHA_ATC_LIB,
						t_ref.PHA_MED_NOM,
					%end;
					%if &opt_xtr_dos. = 1 %then %do;
						t_ref.PHA_UPC_NBR,
						t_ref.PHA_SUB_DOS,
						t_ref.PHA_DOS_UNI,
					%end;
					%if &opt_xtr_psp. = 1 %then %do;
	                    t_prs.PFS_PRE_NUM,
						t_prs.PSP_SPE_COD,
						t_prs.PSP_STJ_COD,
	                    t_prs.PSP_ACT_NAT,
	                    t_prs.PSP_CNV_COD,
	                    t_prs.PSP_PPS_NUM,
					%end;
					%if &opt_xtr_pse. = 1 %then %do;
	                    t_prs.PFS_EXE_NUM,
	                    t_prs.PSE_SPE_COD,
	                    t_prs.PSE_STJ_COD,
	                    t_prs.PSE_ACT_NAT,
	                    t_prs.PSE_CNV_COD,
					%end;
					%if &opt_xtr_cle. = 1 %then %do;
						t_prs.FLX_DIS_DTD,
						t_prs.FLX_TRT_DTD,
						t_prs.FLX_EMT_TYP,
						t_prs.FLX_EMT_NUM,
						t_prs.FLX_EMT_ORD,
						t_prs.ORG_CLE_NUM,
						t_prs.DCT_ORD_NUM,
						t_prs.PRS_ORD_NUM,
						t_prs.REM_TYP_AFF,
					%end;
	 					t_pha.PHA_ACT_QSN	
					from %scan(&jnt_prs_pop., 1,"'") inner join &T_PHA. t_pha on %J9K(t_prs,t_pha)
						inner join &opt_ref. t_ref on t_pha.PHA_PRS_C13 = t_ref.PHA_CIP_C13
							where
								%scan(&flt_atc_cip., 1,"'") and
								(t_prs.EXE_SOI_DTD between to_date(%str(%'&dte_dbt_prs.%'), 'ddmmyyyy') and to_date(%str(%'&dte_fin_prs.%'), 'ddmmyyyy')) and
								(t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') - 1) and
								t_prs.DPN_QLF <> 71)
				)
				BY ORACLE;
			quit;
		%end;
	%end; /*Fermeture annees hors 2006 et 2007*/

	/*2-2-2- Annees 2006 et 2007*/
	%if &aaaa. = 2006 or &aaaa. = 2007 %then %do;
		/*annee de flux N + 27 mois suivants*/
		%do i = 0 %to &limit.;
			%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i),ddmmyyn8.);
			%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

			proc sql;
				%connectora;
				EXECUTE(
				%if %sysfunc(exist(orauser.&tab_out.))=0 %then %do;
					create table &tab_out. as
				%end;
				%else %do;
					insert into &tab_out.
				%end;

				(select t_prs.BEN_NIR_PSA, t_prs.EXE_SOI_DTD, t_ref.PHA_ATC_CLA, t_ref.PHA_PRS_IDE as PHA_CIP_C13,

				%if &opt_xtr_lib. = 1 %then %do;
					t_ref.PHA_ATC_LIB,
					t_ref.PHA_MED_NOM,
				%end;
				%if &opt_xtr_dos. = 1 %then %do;
					t_ref.PHA_UPC_NBR,
					t_ref.PHA_SUB_DOS,
					t_ref.PHA_DOS_UNI,
					t_ref.PHA_GRD_CND,
				%end;
				%if &opt_xtr_psp. = 1 %then
				%do;
					t_prs.PFS_PRE_NUM,
					t_prs.PSP_SPE_COD,
					t_prs.PSP_STJ_COD,
					t_prs.PSP_ACT_NAT,
					t_prs.PSP_CNV_COD,
					t_prs.PSP_PPS_NUM,
				%end;
				%if &opt_xtr_pse. = 1 %then %do;
					t_prs.PFS_EXE_NUM,
					t_prs.PSE_SPE_COD,
					t_prs.PSE_STJ_COD,
					t_prs.PSE_ACT_NAT,
					t_prs.PSE_CNV_COD,
				%end;
				%if &opt_xtr_cle. = 1 %then %do;
					t_prs.FLX_DIS_DTD,
					t_prs.FLX_TRT_DTD,
					t_prs.FLX_EMT_TYP,
					t_prs.FLX_EMT_NUM,
					t_prs.FLX_EMT_ORD,
					t_prs.ORG_CLE_NUM,
					t_prs.DCT_ORD_NUM,
					t_prs.PRS_ORD_NUM,
					t_prs.REM_TYP_AFF,
				%end;
					t_pha.PHA_ACT_QSN
		
				from %scan(&jnt_prs_pop., 1,"'") inner join &T_PHA. t_pha on %J9K(t_prs,t_pha)
					inner join &opt_ref. t_ref on t_pha.PHA_PRS_IDE = t_ref.PHA_PRS_IDE
				where
					%scan(&flt_atc_cip., 1,"'") and
					(t_prs.EXE_SOI_DTD between to_date(%str(%'&dte_dbt_prs.%'), 'ddmmyyyy') and to_date(%str(%'&dte_fin_prs.%'), 'ddmmyyyy')) and
					(t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') - 1) and
					t_prs.DPN_QLF <> 71)

				)
				BY ORACLE;
			quit;
		%end;
	%end; /*Fermeture annees 2006 et 2007*/
%end; /*Fermeture boucle par annee*/


proc sql;
	create table &tab_out. as
	select distinct a.*, sum(a.PHA_ACT_QSN) as PHA_ACT_QSN_bis
		from orauser.&tab_out. a
			group by
				BEN_NIR_PSA,
				EXE_SOI_DTD,
				PHA_ATC_CLA,
				PHA_CIP_C13
			having sum(a.PHA_ACT_QSN) > 0;
quit;

proc sql;
	create table &lib_prj..&tab_out. (drop=PHA_ACT_QSN,rename=(PHA_ACT_QSN_bis=PHA_ACT_QSN)) as
	select *
	from &tab_out.;
quit;

/* Suppression des tables intermediaires */
%if &lib_prj. ne work or &lib_prj. ne WORK %then %do;proc delete data=&tab_out.; run;%end;
%checkDrop(&tab_out.);
%checkDrop(&opt_pop.);
%checkDrop(IR_PHA_R_EXTRACT);

%Stop_macro: /* Fin du programme */
%mend;
