/************************************************************************************************
 *	Macro   : xtr_ucd_dcir																		*
 *	*********************************************************************************************
 *	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219				*
 *	*********************************************************************************************
 *	Mises a jour : (le cas echeant : preciser date, auteur, et objet de la MAJ)					*
 *		08/06/2023 - CM : Creation de la macro %xtr_ucd_dcir								 	*
 *	*********************************************************************************************
 *	Description : Cette macro permet d extraire, dans les donnees du DCIR et durant une 		*
 *              periode de temps donnee, les medicaments delivres en sus ou retrocedes			*
 *				identifies a partir des codes UCD.												*
 *				Si aucun code UCD n est renseigne, la macro extrait toutes les delivrances de	*
 *				medicament sur la periode donnee. 												*
 *	*********************************************************************************************
 *	Parametres :																				*
 *		lib_out 	: nom de la bibliotheque ou sera rangee la table de sortie					*
 *		tab_out 	: nom de la table de sortie													*
 *		dte_dbt_prs  : date de debut de la periode d'extraction (format DD/MM/YYYY)				*
 *		dte_fin_prs  : date de fin de la periode d'extraction (format DD/MM/YYYY)				*
 *		opt_pop 	: optionnel : table de population a "jointer" (ex. orauser.POP)				*
 *		opt_ucd 	: optionnel : liste des codes UCD a extraire (7 caracteres)					*
 *		opt_med_ucd	: optionnel : Extraction medicaments retrocedes, de la liste en sus ou des	*	
 *									deux (0=retrocedes,1=en sus,2=les deux)						*
 *		opt_xtr_dos	: optionnel : extraire les donnees des doses/unites (valeur : 1-oui)		*
 *		opt_xtr_psp	: optionnel : extraire les donnees des prescripteurs (valeur : 1-oui)		*
 *		opt_xtr_pse	: optionnel : extraire les donnees des executants (valeur : 1-oui)			*
 *		opt_xtr_cle	: optionnel : extraire les donnees des cles de jointure (valeur : 1-oui)	*
 *	*********************************************************************************************
 *	Remarques :																					*
 *		Pour extraire des donnees de certains medicaments retrocedes, preciser tous les codes 	*
 *			UCD d interet dans le parametre opt_ucd car il n est actuellement pas possible de	*
 *			selectionner ces donnees a partir du code ATC du medicament (absence de table de 	*
 *			correspondance fiable sur le portail entre les codes UCD et ATC)					*
 *																								*
 *		Les dates doivent obligatoirement faire 10 caracteres, avec les deux slashs. Ainsi		*
 *			m�me les premiers jours du mois, ou les premiers mois de l'ann�e doivent contenir 	*
 *			le 0 comme 01/03/2022.																*
 *	*********************************************************************************************
 *	Exemple :																					*
 *		%xtr_ucd_dcir(																			*
 *			lib_out 	= WORK,																	*
 *			tab_out 	= TEST_PHA,																*
 *			dte_dbt_prs	= 01/05/2009,															*
 *			dte_fin_prs = 31/12/2013,															*
 *			opt_pop 	= ORAUSER.POP_DT2,														*
 *			opt_ucd 	= 9298142,																*
 *			opt_med_ucd = 2,																	*
 *			opt_xtr_dos = 1,																	*
 *			opt_xtr_psp = 1,																	*
 *			opt_xtr_pse = 1,																	*
 *			opt_xtr_cle = ,																		*
 *		Cet exemple permet d'extraire les sujets ayant re�us une d�livrance en r�trocession 	*	
 *		et/ou en sus de m�dicaments cod�s 9298142. La requ�te exrtrait les donn�es des sujets	*
 *		identifi�s dans la table pop_dt2, entre le 01/05/2009 et le 31/12/2013. Les donn�es		*
 *		doses/unites, sur les prescripteurs et ex�cutants sont aussi extraites, et enregistr�es	*
 *		dans la table TEST_PHA sur la Work														*
 ************************************************************************************************/
 *	Am�liorations possibles :																	*
 *		- Retravailler la requ�te sur les codes UCD - ne pas passer par un substr du UC_UC_COD	*
 ************************************************************************************************/
options nosource nonotes nosymbolgen;
option fullstimer debug=dbms_timers sastrace=',,,dq' sastraceloc=saslog nostsuffix msglevel=I;


%macro xtr_ucd_dcir(lib_out, tab_out, ann_deb, ann_fin, dte_dbt_prs, dte_fin_prs, opt_pop, opt_ucd, opt_med_ucd, opt_xtr_dos, opt_xtr_psp, opt_xtr_pse, opt_xtr_cle);
	/*Macro suppression tables temporaires*/
%macro checkDrop(myTable);
	%if %SYSFUNC(exist(orauser.&myTable)) %then
		%do;

			proc sql;
				%connectora;
				EXECUTE(
					drop table &myTable
						)
					BY ORACLE;
			quit;

		%end;
%mend;

/*D�finition auto ann�e archivage donn�es prestation*/
	%let ann_act = %sysfunc(year(%sysfunc(today())));
	%let aa = 2006;
	%let exist = 1;
	%do %while (&exist. = 1);
		%if %sysfunc(exist(oravue.er_prs_f_&aa.))=1 %then %do;
			%let exist = 1;
			%let aa = %eval(&aa.+1);
		%end;
		%else %do;
			%let exist = 0;
		%end;
	%end;
	%let ann_arc = %eval(&aa.-1);


/*Copie table */
/* 1 - Initialisation des tables a jointer */
%if %length(&opt_pop.) > 0 %then
	%do;
		/*Copie table princeps vers orauser*/
		%if %sysfunc(exist(orauser.&opt_pop.))=0 %then
			%do;
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
						as select * from &lib_out..&opt_pop.;
				quit;
			%end;
		%else
			%do;
				%checkDrop(&opt_pop.);
				proc sql;
					create table orauser.&opt_pop. (BULKLOAD=yes BL_DATAFILE="%sysfunc(pathname(work))/data.dat" BL_DELETE_DATAFILE=yes) 
						as select * from &lib_out..&opt_pop.;
				quit;
			%end;
		%let jnt_prs_pop = '&opt_pop. t_pop inner join &T_PRS. t_prs on t_pop.BEN_NIR_PSA = t_prs.BEN_NIR_PSA';
	%end;
%else
	%do;
		%let jnt_prs_pop = '&T_PRS. t_prs';
	%end;

	
/* Macro ANSM pour jointure des 9 tables avec cle primaire */
%macro J9K(G,D,HELP=NO);
	/***
	Jointure sur les 9 cles entre 2 tables de DCIR (DCT exclus)
	"ne vous demandez pas ce que votre jointure peut faire pour vous,
	mais demandez-vous ce que vous voulez faire avec votre jointure" ;-)
	Source profil 69 : &Fichiers/ANSM/Macros/j9k.sas
	***/
	%if %upcase(&HELP)=YES %then
		%do;

			data _null_;
				put
					'Macro J9K: Jointure sur les 9 cles entre 2 tables de DCIR (DCT exclus)      '/
					"- G      : Alias de la table de gauche                                      "/
					"- D      : Alias de la table de droite                                      "/
					"- HELP   : Pour avoir cette aide, par defaut : NO                           "/
					"           L'aide neutralise l'execution                                    "/
					"Exemple d'utilisation : a introduire dans un PROC SQL                       "/
					'  select liste_variables_groupe, liste_variables_agregees                   '/
					'  from ORAVUE.ER_PRS_F as PRS inner join ORAVUE.ER_ETE_F as ETE             '/
					'  on %J9K(PRS,ETE)                                                          '/
					'  where filtre_fonctiontionnel                                              '/
					'  group by liste_variables_groupe                                           '/
					'  ;                                                                         '/
					'Exemple d''utilisation : a introduire dans un PROC SQL + %CONNECTORA        '/
					'  select liste_variables_groupe, liste_variables_agregees                   '/
					'  from ER_PRS_F PRS left join ER_ETE_F ETE                                  '/
					'  on %J9K(PRS,ETE)                                                          '/
					'  where filtre_fonctiontionnel                                              '/
					'  group by liste_variables_groupe                                           '/
					'  ;                                                                         '/
				;
			run;

			%return;
		%end;

	&G..DCT_ORD_NUM=&D..DCT_ORD_NUM AND &G..FLX_DIS_DTD=&D..FLX_DIS_DTD AND &G..FLX_EMT_ORD=&D..FLX_EMT_ORD AND
		&G..FLX_EMT_NUM=&D..FLX_EMT_NUM AND &G..FLX_EMT_TYP=&D..FLX_EMT_TYP AND &G..FLX_TRT_DTD=&D..FLX_TRT_DTD AND
		&G..ORG_CLE_NUM=&D..ORG_CLE_NUM AND &G..PRS_ORD_NUM=&D..PRS_ORD_NUM AND &G..REM_TYP_AFF=&D..REM_TYP_AFF
%mend J9K;

/* Macro necessaire pour extraction des codes ATC/CIP */
%macro multi_like_pha(var, lst_cod, aim, type);
	%local i val ope;
	%let i = 1;
	%let val =;
	%let ope =;

	%do %until (%superq(val) =);
		%let val = %qscan(&lst_cod., &i., %str( ));

		%if &aim. = INC %then
			%do;
				%if %superq(val) > 0 %then
					%do;
						%cmpres(%str(&ope. %upcase(&var.) like %'%upcase(&val.)%%%'))
					%end;

				%put %str(&ope. %upcase(&var.) like %'%upcase(%superq(val))%%%');
				%let ope = or;
			%end;

		%if &aim. = EXC %then
			%do;
				%if %superq(val) > 0 %then
					%do;
						%cmpres(%str(&ope. %upcase(&var.) not like %'%upcase(&val.)%%%'))
					%end;

				%put %str(&ope. %upcase(&var.) not like %'%upcase(%superq(val))%%%');
				%let ope = and;
			%end;

		%let i = %eval(&i. + 1);
	%end;
%mend;

	/* Filtres des codes UCD de la pharmacie hospitaliere */
	%if %length(&opt_ucd.) > 0 %then
		%do;
			%let flt_ucd = '(%multi_like_pha(var = to_char(substr(UCD_UCD_COD,7,7)), lst_cod = &opt_ucd., aim = INC))';
		%end;
	%else
		%do;
			%let flt_ucd = 'UCD_UCD_COD is not null';
		%end;

	/* Filtre pour identification des medicaments retrocedes, en sus ou les deux */
	%if &opt_med_ucd. = 0 %then
		%do;
			/*Retrocession*/
			%let flt_med_ucd = 'UCD_TOP_UCD = 0';
		%end;
	%else %if &opt_med_ucd. = 1 %then
		%do;
			/*En sus*/
			%let flt_med_ucd = 'UCD_TOP_UCD = 1';
		%end;
	%else %if &opt_med_ucd. = 2 %then
		%do;
			/*Les deux*/
			%let flt_med_ucd = '(UCD_TOP_UCD = 0 or UCD_TOP_UCD = 1)';
		%end;


	/* Initilisation de la table extraction */
	%let tab_xtr_ucd = &tab_out._UCD;
	%checkDrop(&tab_xtr_ucd.);


	/* Initilisation desnvaleurs d'extraction */
	%let ann_deb = %sysfunc(substr(&dte_dbt_prs.,7,4));
	%let ann_fin = %sysfunc(substr(&dte_fin_prs.,7,4));
	%let dte_dbt_prs = %sysfunc(compress(&dte_dbt_prs., '/'));
	%let dte_fin_prs = %sysfunc(compress(&dte_fin_prs., '/'));

/**************************************************************************/
/* Recherche des medicaments dans les donnees de retrocession et hors GHS */
/**************************************************************************/


	%do aaaa = &ann_deb. %to &ann_fin.;

		%if &aaaa. <= &ann_arc. %then
		%do;
			%let limit = 13;
		%end;
		%else
		%do;
			%let limit = 38;
		%end;

		%if &aaaa. <= &ann_arc. %then
			%do;
				%let T_PRS = ER_PRS_F_&aaaa.;
				%let T_UCD = ER_UCD_F_&aaaa.;
			%end;
		%else
			%do;
				%let T_PRS = ER_PRS_F;
				%let T_UCD = ER_UCD_F;
			%end;

		/*annee de flux N + 27 mois suivants*/

		%do i = 0 %to &limit.;
			%let flxDbt=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),&i.),ddmmyyn8.);
			%let flxFin=%sysfunc(intnx(MONTH,%sysfunc(inputn(%sysfunc(mdy(1,1,&aaaa.),ddmmyyn8.),ddmmyy8.)),%eval(&i.+1)),ddmmyyn8.);

			proc sql;
				%connectora;
				EXECUTE(
					%if (%sysfunc(exist(orauser.&tab_xtr_ucd.))=0) %then
						%do;
							create table &tab_xtr_ucd. as
						%end;
					%else
						%do;
							insert into &tab_xtr_ucd.
						%end;

					(select distinct t_prs.BEN_NIR_PSA, t_prs.EXE_SOI_DTD, substr(t_ucd.UCD_UCD_COD,7,7) as UCD_UCD_COD,

					%if &opt_xtr_dos. = 1 %then
						%do;
							t_ucd.UCD_DLV_NBR as PHA_UPC_NBR,
						%end;

					%if &opt_xtr_psp. = 1 %then
						%do;
                        	t_prs.PFS_PRE_NUM,
							t_prs.PSP_SPE_COD,
							t_prs.PSP_STJ_COD,
                        	t_prs.PSP_ACT_NAT,
                        	t_prs.PSP_CNV_COD,
                        	t_prs.PSP_PPS_NUM,
						%end;
					
					%if &opt_xtr_pse. = 1 %then
						%do;
                        	t_prs.PFS_EXE_NUM,
                        	t_prs.PSE_SPE_COD,
                        	t_prs.PSE_STJ_COD,
                        	t_prs.PSE_ACT_NAT,
                        	t_prs.PSE_CNV_COD,
						%end;

					%if &opt_xtr_cle. = 1 %then
						%do;
							t_prs.FLX_DIS_DTD,
							t_prs.FLX_TRT_DTD,
							t_prs.FLX_EMT_TYP,
							t_prs.FLX_EMT_NUM,
							t_prs.FLX_EMT_ORD,
							t_prs.ORG_CLE_NUM,
							t_prs.DCT_ORD_NUM,
							t_prs.PRS_ORD_NUM,
							t_prs.REM_TYP_AFF,
						%end;
					t_ucd.UCD_TOP_UCD /*distinguer med retrocedes et ceux de la liste en sus */
				from %scan(&jnt_prs_pop., 1, "'") inner join &T_UCD t_ucd on %J9K(t_prs,t_ucd)
					where
						%scan(&flt_ucd., 1,"'") and
						(t_prs.EXE_SOI_DTD between to_date(%str(%'&dte_dbt_prs.%'),'ddmmyyyy') and to_date(%str(%'&dte_fin_prs.%'),'ddmmyyyy') - 1) and
						(t_prs.FLX_DIS_DTD between to_date(%str(%'&flxDbt.%'), 'ddmmyyyy') and to_date(%str(%'&flxFin.%'), 'ddmmyyyy') - 1) and
						t_prs.DPN_QLF <> 71 and
						t_ucd.UCD_DLV_NBR > 0 and
						%scan(&flt_med_ucd., 1, "'")))
					BY ORACLE;
						quit;

		%end;
	%end;

		/*Definir type prestation selon UCD_TOP_UCD*/
		data &lib_out..&tab_xtr_ucd.;
			set orauser.&tab_xtr_ucd.;
			if UCD_TOP_UCD = 1 then
				PRS_TYP = 'SUS';
			else PRS_TYP = 'RETRO';
		run;


		%checkDrop(&tab_xtr_ucd.);


		proc sort data=&lib_out..&tab_xtr_ucd. nodupkey; by BEN_NIR_PSA; run;

%checkDrop(&opt_pop.);
%mend;

