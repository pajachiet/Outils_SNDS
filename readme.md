# Outils SNDS

*Présentation en quelques lignes du contenu du dépôt*

## Présentation

- *Langage*
- *Où utiliser ces programmes*
- *Avec quel accès*
- *Schéma / image de présentation*
- *Organisation des dossiers*
- *Comment s'en servir*

## Références utiles

- *Publications ayant utilisé ces outils*
- *Sources d'informations complémentaires*
- *Tutoriels*

## Auteur

- *Equipe et contributeurs*
- *Contact*

## Licence

Ce projet, les macros et documentations sont sous licence Apache 2.0.