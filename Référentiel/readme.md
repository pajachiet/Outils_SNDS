# Référentiel
Version du 6 octobre 2023

_Le référentiel est sous licence Apache 2.0._


## 1. Récupération de tous les identifiants disponibles dans le SNDS

Cette macro permet d'extraire tous les sujets identifies dans le SNDS.

Importation de tous les identifiants issus des tables IR\_BEN\_R et IR\_BEN\_R\_ARC

## 2. Identification de tous les identifiants du PMSI/DCIR associés à chaque BEN\_IDT\_ANO

Création de la table REF\_NIR\_OPT pour identifier les identifiants en doublon et changements de NIR tout au long du suivi du DCIR. Les jumeaux/jumelles sont supprimés de ce référentiel pour question de simplification des requêtes

## 3. Association de tous les couples d'identifiants sous un identifiant unique créé pour l'occasion

Création de la table REF\_NIR\_CLE pour identifier les associations des identifiants entre BEN\_IDT\_ANO, BEN\_NIR\_PSA (IDs du portail), et BEN\_NIR\_OPT (ID de DRUGS-SAFEr).

## 4. Correction des anomalies de saisie et suppression des identifiants trop problématiques

Correction des anomalies socio-démographiques avec la réalisation de la table REF\_NIR\_DEM

Vérification des âges et sexe mal saisies ou trop changeants

Suppression des identifiants avec trop de couples d'identifiants

## 5. Correction des anomalies liées au décès

Correction des anomalies liées au décès avec la réalisation de la table REF\_NIR\_DC

## 6. Réalisation d'un référentiel de présence annuel pour chaque BEN\_NIR\_OPT entre 2009 et 2021

Jointures (à gauche) entre chaque table EXTRACTION\_anneeTR, avec l'identifiant unique NIR\_OPT, créant une table rassemblant les informations annuelles respectives : booléen indiquant une affiliation dans l'année, la date de dernier soin de l'année, l'information du Régime, la CMU-c et l'AME.