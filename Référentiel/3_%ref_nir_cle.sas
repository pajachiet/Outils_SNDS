
/****************************************************************************************************************
*	Macro  : ref_nir_cle															    						*
*	Date   : 02/10/2023																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises � jour : (le cas �ch�ant : pr�ciser date et objet de la MAJ)											*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet de cr�er la table rassemblant toutes les combinaisons indiquees pour un	*
*	identifiant BEN_IDT_ANO, � partir d'une extraction princeps de sujets. Cette table princeps est une table	*
*	de BEN_NIR_PSA initialement cr��e (table de sujets ayant une prescription d'un m�dicament, hospitalis� pour	*
*	une maladie, etc.). La table princeps est obligatoire, doit �tre stock�e la librairie du projet.			*
*	*************************************************************************************************************
*	Param�tres :																								*
*		lib_prj 	: nom de la biblioth�que o� sera rang�e la table de sortie									*
*		tab_xtr 	: nom de la table princeps. Elle est obligatoire et doit �tre stock�e dans la librairie 	*
*					  mentionn�e avec l'option lib_prj.															*
*	*************************************************************************************************************
*	R�sultat : table contenant les variables suivantes :														*
*		BEN_NIR_OPT : Nouvel identifiant cr��, anonymis�														*
*		BEN_NIR_PSA : Identifiant anonyme du patient dans le SNIIRAM											*
*		BEN_IDT_ANO : Identifiant b�n�ficiaire anonymis�														*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_nir_cle(lib_prj = LIBABCD, tab_xtr=Princeps_Diabete_1520);												*
*	Cet exemple permet d'identifier toutes les combinaisons indiqu�es pour un m�me identifiant BEN_IDT_ANO		*
*	issu de la table Princeps_Diabete_1520 (exemple de sujets ayant eu un traitement antidiab�tique entre 2015	*
* 	et 2020). La table de sortie est enregistr�e dans la libraire LIBABCD.										*
****************************************************************************************************************/

%macro ref_nir_cle(lib_prj, tab_xtr);

	/* 1 - Initialisation */
	proc sql;
		drop table &lib_prj..REF_NIR_CLE;
		drop table ORAUSER.REF_NIR_CLE;
	quit;


	/* 2 - Cr�ation de la table REF_NIR_CLE */
	/***
		S�lection des BEN_NIR_OPT associ�s aux BEN_NIR_PSA issus de l'extraction princeps
		+ R�cup�ration de tous les autres BEN_NIR_PSA associ�s aux BEN_NIR_OPT s�lectionn�s
	***/
	proc sql;
		create table &lib_prj..REF_NIR_CLE as
		select  distinct t3.BEN_NIR_OPT,
						 t3.BEN_NIR_PSA,
						 t3.BEN_IDT_ANO
		from &lib_prj..&tab_xtr. t1 inner join &lib_prj..REF_NIR_OPT  t2 on t1.BEN_NIR_PSA = t2.BEN_NIR_PSA
								  	inner join &lib_prj..REF_NIR_OPT  t3 on t2.BEN_NIR_OPT = t3.BEN_NIR_OPT;

		create table ORAUSER.REF_NIR_CLE as
		select *
		from &lib_prj..REF_NIR_CLE;
	quit;

%mend;

%ref_nir_cle(lib_prj = , tab_xtr = );
