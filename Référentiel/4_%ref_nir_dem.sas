
/****************************************************************************************************************
*	Macro  : ref_nir_dem															    						*
*	Date   : 03/10/2023																							*
*	*************************************************************************************************************
*	Apache License : Copyright 2023, AHeaD Team Bordeaux Population Health U1219								*
*	*************************************************************************************************************
*	Mises � jour : (le cas �ch�ant : pr�ciser date et objet de la MAJ)											*
*																												*
*	*************************************************************************************************************
*	Description : Cette macro permet de cr�er la table rassemblant le sexe et les mois/ann�e de naissance pour 	*
*	chaque patient.																								*
*	*************************************************************************************************************
*	Param�tres :																								*
*		lib_prj 	: nom de la biblioth�que o� sera rang�e la table de sortie									*
*	*************************************************************************************************************
*	R�sultat : table contenant les variables suivantes :														*
*		BEN_NIR_OPT : Nouvel identifiant cr��, anonymis�														*
*		BEN_SEX_COD : Sexe																						*
*		BEN_NAI_ANN : Ann�e de naissance																		*
*		BEN_NAI_MOI : Mois de naissance																			*
*****************************************************************************************************************
* 	Exemple	:																									*
* 	%ref_nir_dem(lib_prj = LIBABCD);																			*
*	Cet exemple permet de d�finir le sexe et mois/ann�e de naissance pour chaque patient de la population 		*
*	issue de votre table REF_NIR_CLE. La table de sortie est enregistr�e dans la libraire LIBABCD.				*
*																												*
*	La proc�dure proc freq permet de connaitre les effectifs disponibles/exlus :								*
*		dem_excl : nombre total de sujets exclus pour donn�es incoh�rentes/manquantes 							*
*					sur sexe et/ou mois de naissance et/ou ann�e de naissance 									*
*		doubl_excl : nombre de sujets exclus pour donn�es "en double" et non identiques entre sexe 				*
*						et/ou mois de naissance et/ou ann�e de naissance 										*
*		BEN_SEX_COD_excl : nombre de sujets exclus pour donn�es incoh�rentes/manquantes sur le sexe				*
*							(0- si modalit�s [1;2] ; 1-sinon)													*
*		BEN_NAI_ANN_excl : nombre de sujets exclus pour donn�es incoh�rentes/manquantes sur l'ann�e de naiss 	*
*							(0- ann�e naissance >=1900 ; 1-sinon)												*
*		BEN_NAI_MOI_c_excl : nombre de sujets exclus pour donn�es incoh�rentes/manquantes sur le mois de naiss 	*
*							(0- si modalit�s [1 � 12] ; 1-sinon)												*
****************************************************************************************************************/


%macro ref_nir_dem(lib_prj);

/* 1 - Initialisation */
	proc sql;
		drop table &lib_prj..REF_NIR_DEM;
	quit;

/* 2 - Informations sur le sexe et la date de naissance */

	proc sql;
		create table &lib_prj..REF_NIR_DEM as
		select distinct t1.BEN_NIR_OPT,
						t2.BEN_SEX_COD,
						t2.BEN_NAI_ANN,
						t2.BEN_NAI_MOI
		from &lib_prj..REF_NIR_CLE t1 inner join &lib_prj..REF_IR_BEN t2 
							on t1.BEN_IDT_ANO = t2.BEN_IDT_ANO; 

/* 3 - Gestion des valeurs aberrantes */

	data &lib_prj..REF_NIR_DEM;
		set &lib_prj..REF_NIR_DEM;
		format BEN_NAI_MOI_c $10.;
		if BEN_NAI_MOI in ("01","1") then BEN_NAI_MOI_c="1";
		else if BEN_NAI_MOI in ("02","2") then BEN_NAI_MOI_c="2";
		else if BEN_NAI_MOI in ("03","3") then BEN_NAI_MOI_c="3";
		else if BEN_NAI_MOI in ("04","4") then BEN_NAI_MOI_c="4";
		else if BEN_NAI_MOI in ("05","5") then BEN_NAI_MOI_c="5";
		else if BEN_NAI_MOI in ("06","6") then BEN_NAI_MOI_c="6";
		else if BEN_NAI_MOI in ("07","7") then BEN_NAI_MOI_c="7";
		else if BEN_NAI_MOI in ("08","8") then BEN_NAI_MOI_c="8";
		else if BEN_NAI_MOI in ("09","9") then BEN_NAI_MOI_c="9";
		else if BEN_NAI_MOI in ("10") then BEN_NAI_MOI_c="10";
		else if BEN_NAI_MOI in ("11") then BEN_NAI_MOI_c="11";
		else if BEN_NAI_MOI in ("12") then BEN_NAI_MOI_c="12";
		run;

	* ajout du nombre de lignes concernant un m�me et unique BEN_NIR_OPT (valeurs multiples) ;
	proc sql;
		create table cnt as
		select distinct BEN_NIR_OPT, count(BEN_NIR_OPT) as cnt
		from &lib_prj..REF_NIR_DEM
		group by BEN_NIR_OPT;
		quit;

	proc sql;
		create table &lib_prj..REF_NIR_DEM as
		select distinct *
		from &lib_prj..REF_NIR_DEM t1 left join cnt t2 on t1.BEN_NIR_OPT = t2.BEN_NIR_OPT;
		quit;

	data &lib_prj..REF_NIR_DEM;
		set &lib_prj..REF_NIR_DEM;
		if cnt=1 then doubl_excl=0; else doubl_excl=1;
		if BEN_SEX_COD in (1,2) then BEN_SEX_COD_excl=0; else BEN_SEX_COD_excl=1;
		if (BEN_NAI_ANN>=1900 and BEN_NAI_ANN<=%sysfunc(year(%sysfunc(today())))) then BEN_NAI_ANN_excl=0; else BEN_NAI_ANN_excl=1;
		if BEN_NAI_MOI_c in (1,2,3,4,5,6,7,8,9,10,11,12) then BEN_NAI_MOI_c_excl=0; else BEN_NAI_MOI_c_excl=1;
		if doubl_excl=1 or BEN_SEX_COD_excl=1 or BEN_NAI_ANN_excl=1 or BEN_NAI_MOI_c_excl=1 then dem_excl=1; else dem_excl=0;
		run;

	proc freq data=LIBRFLU.REF_NIR_DEM;
		tables dem_excl doubl_excl BEN_SEX_COD_excl BEN_NAI_ANN_excl BEN_NAI_MOI_c_excl;
		run;

	proc sql;
		create table &lib_prj..REF_NIR_DEM as
		select distinct BEN_NIR_OPT, BEN_SEX_COD, BEN_NAI_ANN, BEN_NAI_MOI_c as BEN_NAI_MOI
		from &lib_prj..REF_NIR_DEM
		where dem_excl=0;
		quit;

%mend;

%ref_nir_dem(lib_prj = );
